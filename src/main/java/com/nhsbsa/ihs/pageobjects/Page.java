package com.nhsbsa.ihs.pageobjects;

import io.cucumber.java.Scenario;
import org.openqa.selenium.*;
import org.openqa.selenium.support.ui.ExpectedConditions;
import org.openqa.selenium.support.ui.Wait;
import org.openqa.selenium.support.ui.WebDriverWait;
import org.openqa.selenium.By;

import java.time.Duration;
import java.util.NoSuchElementException;
import java.util.Random;
import java.util.function.Function;

public class Page {

    protected WebDriver driver;
    private WebElement element;
    public int explicitWaitTime = 50;
    public WebDriverWait wait;

    public Page(WebDriver driver) {
        this.driver = driver;
    }

    public void navigateToUrl(String url) {
        driver.get(url);
    }

    public WebDriverWait getWait() {
        wait = new WebDriverWait(driver, Duration.ofSeconds(explicitWaitTime));
        wait.ignoring(NoSuchElementException.class);
        wait.ignoring(StaleElementReferenceException.class);
        wait.ignoring(ElementNotVisibleException.class);
        return wait;
    }

    public void waitForPageLoad() {
        Wait<WebDriver> wait = new WebDriverWait(driver, Duration.ofSeconds(explicitWaitTime));
        wait.until(new Function<WebDriver, Boolean>() {
            public Boolean apply(WebDriver driver) {
                ((JavascriptExecutor) driver).executeScript("return document.readyState");
                return (((JavascriptExecutor) driver).executeScript("return document.readyState"))
                        .equals("complete");
            }
        });
    }

    public void clickEvent(By by) {
        waitForPageLoad();
        wait = getWait();
        WebElement element = wait.until(ExpectedConditions.presenceOfElementLocated(by));
        element.click();
    }

    public void sendTextValues(By by, String text) {
        waitForPageLoad();
        wait = getWait();
        WebElement element = wait.until(ExpectedConditions.presenceOfElementLocated(by));
        element.clear();
        element.sendKeys(text);
    }

    public void checkScenarioRunStatus(Scenario scenario) {
        if (scenario.isFailed()) {
            final byte[] screenshot = ((TakesScreenshot) driver)
                    .getScreenshotAs(OutputType.BYTES);
            scenario.attach(screenshot,"image/jpeg", "failed screen" );
        }
    }

    public String getElementText() {
        return element.getText();
    }

    public String getElementText(By by) {
        return driver.findElement(by).getText();
    }

    public String generateOverLimitText(int count) {
        char[] chars = "abcdefghijklmnopqrstuvwxyz".toCharArray();
        StringBuilder sb = new StringBuilder(20);
        Random random = new Random();
        for (int i = 0; i < count; i++) {
            char c = chars[random.nextInt(chars.length)];
            sb.append(c);
        }
        String generateOverLimitText = sb.toString();
        return generateOverLimitText;
    }

    public int getLengthOfEnteredText(By by) {
        return driver.findElement(by).getAttribute("value").length();
    }

    public String getPageTitles(){
        return driver.getTitle();
    }

    public void click() {
        element.click();
    }

    public void tearDownDriver() {
        driver.quit();
    }

    public String getUrl() {
        return driver.getCurrentUrl();
    }

    public void goBack() {
        driver.navigate().back();
    }

    public void navigateToElementBy(By by) {
        element = element.findElement(by);
    }

    public String getAttributeValue(String val) {
        return element.getAttribute(val);
    }

    public String getElementValue(By by) {
        return driver.findElement(by).getAttribute("value");
    }

    public void clickEventJS(By by){
        JavascriptExecutor js = (JavascriptExecutor)driver;
        WebElement element = driver.findElement(by);
        js.executeAsyncScript("window.setTimeout(arguments[arguments.length - 1], 500);");
        element.click();
    }

    public void sendTextValuesJS(By by, String text) {
        JavascriptExecutor js = (JavascriptExecutor)driver;
        WebElement element = driver.findElement(by);
        js.executeAsyncScript("window.setTimeout(arguments[arguments.length - 1], 500);");
        element.clear();
        element.sendKeys(text);
    }
}
