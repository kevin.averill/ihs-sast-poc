package com.nhsbsa.ihs.pageobjects;

import org.openqa.selenium.By;
import org.openqa.selenium.WebDriver;

public class UploadDependantEHICFilePage extends Page {

    private By uploadDependantEHICButtonLocator = By.id("upload-dependant-european-health-insurance-card-ehic");
    private By continueButtonLocator = By.id("continue-button");
    private By errorMessageLocator = By.partialLinkText("Select a file");
    private By uploadedEHICLocator = By.xpath("//*[@id=\"uploaded-dependant-files-european-health-insurance-card-ehic\"]/dl/div/dd");

    public UploadDependantEHICFilePage(WebDriver driver) {
        super(driver);
    }

    public void continueButton() {
        clickEvent(continueButtonLocator);
    }

    public void chooseDependantEHICAndContinue(String fileName) {
        sendTextValues(uploadDependantEHICButtonLocator, fileName);
        continueButton();
    }

    public String getUploadErrorMessage() {
        String getUploadErrorMessage = getElementText(errorMessageLocator);
        return getUploadErrorMessage;
    }

    public String getUploadedEHIC() {
        String getUploadedFile = getElementText(uploadedEHICLocator);
        return getUploadedFile;
    }

    public void continueOnViewEHIC() {
        clickEvent(continueButtonLocator);
    }
}
