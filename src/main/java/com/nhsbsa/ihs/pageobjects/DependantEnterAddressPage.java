package com.nhsbsa.ihs.pageobjects;

import org.openqa.selenium.By;
import org.openqa.selenium.WebDriver;

public class DependantEnterAddressPage extends Page {

    private By buildingAndStreetLocator1 = By.id("dependant-address-line-1");
    private By buildingAndStreetLocator2 = By.id("dependant-address-line-2");
    private By townOrCityLocator = By.id("dependant-address-town");
    private By countyLocator = By.id("dependant-address-county");
    private By postCodeLocator = By.id("dependant-address-postcode");
    private By continueButtonLocator = By.id("continue-button");
    private By buildingErrorMessageLocator = By.partialLinkText("Enter the dependant's building name");
    private By townErrorMessageLocator = By.partialLinkText("Enter the dependant's town");
    private By countyErrorMessageLocator = By.partialLinkText("Enter the dependant's county");
    private By postCodeErrorMessageLocator = By.partialLinkText("Enter the dependant's postcode");
    private By wrongPostCodeErrorMessageLocator = By.partialLinkText("Enter a");

    public DependantEnterAddressPage(WebDriver driver) {
        super(driver);
    }

    public void continueButton() {
        clickEvent(continueButtonLocator);
    }

    public void enterAddressAndSubmit(String building, String street, String town, String county, String postCode) {
        sendTextValues(buildingAndStreetLocator1, building);
        sendTextValues(buildingAndStreetLocator2, street);
        sendTextValues(townOrCityLocator, town);
        sendTextValues(countyLocator, county);
        sendTextValues(postCodeLocator, postCode);
        continueButton();
    }

    public String getBuildingErrorMessage() {
        String getBuildingErrorMessage = getElementText(buildingErrorMessageLocator);
        return getBuildingErrorMessage;
    }

    public String getTownErrorMessage() {
        String getTownErrorMessage = getElementText(townErrorMessageLocator);
        return getTownErrorMessage;
    }

    public String getCountyErrorMessage() {
        String getCountyErrorMessage = getElementText(countyErrorMessageLocator);
        return getCountyErrorMessage;
    }

    public String getPostCodeErrorMessage() {
        String getPostCodeErrorMessage = getElementText(postCodeErrorMessageLocator);
        return getPostCodeErrorMessage;
    }

    public String getWrongPostCodeErrorMessage() {
        String getWrongPostCodeErrorMessage = getElementText(wrongPostCodeErrorMessageLocator);
        return getWrongPostCodeErrorMessage;
    }

    public String getEnteredBuilding() { return getElementValue(buildingAndStreetLocator1); }

    public String getEnteredStreet() { return getElementValue(buildingAndStreetLocator2); }

    public String getEnteredTown() { return getElementValue(townOrCityLocator); }

    public String getEnteredCounty() { return getElementValue(countyLocator); }

    public String getEnteredPostCode() { return getElementValue(postCodeLocator); }

    public void enterAddress(String building, String street, String town, String county, String postCode) {
        sendTextValues(buildingAndStreetLocator1, building);
        sendTextValues(buildingAndStreetLocator2, street);
        sendTextValues(townOrCityLocator, town);
        sendTextValues(countyLocator, county);
        sendTextValues(postCodeLocator, postCode);
    }


}
