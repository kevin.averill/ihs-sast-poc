package com.nhsbsa.ihs.pageobjects;

import org.openqa.selenium.By;
import org.openqa.selenium.WebDriver;

public class DependantIHSNumberPage extends Page{

    private By ihsNumberLocator = By.id("dependant-immigration-health-surcharge-number");
    private By continueButtonLocator = By.id("continue-button");
    private By ihsErrorMessageLocator = By.partialLinkText("Enter a correct");
    private By emptyIHSErrorMessageLocator = By.partialLinkText("Enter the dependant's");

    public DependantIHSNumberPage(WebDriver driver) {
        super(driver);
    }

    public void continueButton() {
        clickEvent(continueButtonLocator);
    }

    public void enterIHSNumberAndSubmit(String ihsNumber) {
        sendTextValues(ihsNumberLocator,ihsNumber);
        clickEvent(continueButtonLocator);
    }

    public String getIHSNumberErrorMessage(){
        String getIHSNumberErrorMessage = getElementText(ihsErrorMessageLocator);
        return getIHSNumberErrorMessage;
    }

    public String getEmptyIHSNumberErrorMessage() {
        String getEmptyIHSNumberErrorMessage = getElementText(emptyIHSErrorMessageLocator);
        return getEmptyIHSNumberErrorMessage;
    }

    public String getEnteredIHSNumber() {
        return getElementValue(ihsNumberLocator);
    }

    public void enterIHSNumber(String ihsNumber) {
        sendTextValues(ihsNumberLocator,ihsNumber);
    }

}
