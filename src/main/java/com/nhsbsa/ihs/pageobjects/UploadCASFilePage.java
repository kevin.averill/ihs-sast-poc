package com.nhsbsa.ihs.pageobjects;

import org.openqa.selenium.By;
import org.openqa.selenium.WebDriver;

public class UploadCASFilePage extends Page {

    private By uploadCASButtonLocator = By.id("upload-evidence");
    private By continueButtonLocator = By.id("continue-button");
    private By errorMessageLocator = By.partialLinkText("Select a file");
    private By uploadedEHICLocator = By.xpath("//*[@id=\"uploaded-files-evidence\"]/dl/div[1]/dd");
    private By uploadedCASLocator = By.xpath("//*[@id=\"uploaded-files-evidence\"]/dl/div[2]/dd");

    public UploadCASFilePage(WebDriver driver) {
        super(driver);
    }

    public void continueButton() {
        clickEvent(continueButtonLocator);
    }

    public void chooseCASAndContinue(String fileName) {
        sendTextValues(uploadCASButtonLocator, fileName);
        clickEvent(continueButtonLocator);
    }

    public String getUploadErrorMessage() {
        String getUploadErrorMessage = getElementText(errorMessageLocator);
        return getUploadErrorMessage;
    }

    public String getUploadedEHIC() {
        String getUploadedFile = getElementText(uploadedEHICLocator);
        return getUploadedFile;
    }

    public String getUploadedCAS() {
        String getUploadedFile = getElementText(uploadedCASLocator);
        return getUploadedFile;
    }

    public void continueOnViewCAS() {
        clickEvent(continueButtonLocator);
    }
}
