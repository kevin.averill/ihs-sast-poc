package com.nhsbsa.ihs.pageobjects;

import org.openqa.selenium.By;
import org.openqa.selenium.WebDriver;
import org.openqa.selenium.remote.LocalFileDetector;
import org.openqa.selenium.remote.RemoteWebDriver;

public class UploadEHICFilePage extends Page {

    private By uploadEHICButtonLocator = By.id("upload-european-health-insurance-card-ehic");
    private By continueButtonLocator = By.id("continue-button");
    private By errorMessageLocator = By.partialLinkText("Select a file");
    private By uploadedEHICLocator = By.xpath("//*[@id=\"uploaded-files-european-health-insurance-card-ehic\"]/dl/div/dd");

    public UploadEHICFilePage(WebDriver driver) {
        super(driver);
    }

    public void continueButton() {
        clickEvent(continueButtonLocator);
    }

    public void chooseEHICAndContinue(String fileName) {
        sendTextValues(uploadEHICButtonLocator, fileName);
        clickEvent(continueButtonLocator);
    }

    public String getUploadErrorMessage() {
        String getUploadErrorMessage = getElementText(errorMessageLocator);
        return getUploadErrorMessage;
    }

    public String getUploadedEHIC() {
        String getUploadedFile = getElementText(uploadedEHICLocator);
        return getUploadedFile;
    }

    public void continueOnViewEHIC() {
        clickEvent(continueButtonLocator);
    }
}
