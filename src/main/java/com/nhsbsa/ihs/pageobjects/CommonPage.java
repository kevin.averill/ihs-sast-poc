package com.nhsbsa.ihs.pageobjects;

import org.openqa.selenium.By;
import org.openqa.selenium.WebDriver;

import java.util.ArrayList;
import java.util.List;

public class CommonPage extends Page {

    private By govUKLinkLocator = By.id("logo");
    private By serviceNameLocator = By.id("proposition-name");
    private By backLinkLocator = By.id("step");
    private By confirmAndSendLocator = By.id("continue-button");
    private By moreInfoOnDeclarationLocator = By.partialLinkText("information you give may be");
    private By acceptAndSendLocator = By.id("continue-button");
    private By contactOnConfirmationLocator = By.partialLinkText("contact");
    private By feedbackLinkLocator = By.partialLinkText("What did you");
    private By referenceLocator = By.xpath("//*[@id=\"gov-grid-row-content\"]/div/form/div/div/strong");
    private By cookiesLocator = By.id("cookie-link");
    private By accessibilityStatementLocator = By.id("accessibility-link");
    private By helpLocator = By.id("help-link");
    private By contactUsLocator = By.id("contact-link");
    private By termsConditionsLocator = By.id("terms-and-conditions-link");
    private By privacyNoticeLocator = By.id("privacy-link");
    private By copyrightLogoLocator = By.id("copyright-logo");
    private By openLicenceLocator = By.id("open-government-licence");

    public CommonPage(WebDriver driver) {
        super(driver);
    }

    public void govUKLink() {
        clickEvent(govUKLinkLocator);
    }

    public void serviceNameLink() {
        clickEvent(serviceNameLocator);
    }

    public void backLink() {
        clickEvent(backLinkLocator);
    }

    public void confirmOnCheckAnswers() {
        clickEvent(confirmAndSendLocator);
    }

    public void acceptOnDeclaration() {
        clickEvent(acceptAndSendLocator);
    }

    public void declarationPrivacy() {
        clickEvent(moreInfoOnDeclarationLocator);
    }

    public void confirmationContact() {
        clickEvent(contactOnConfirmationLocator);
    }

    public void navigateToFeedbackForm() {
        clickEvent(feedbackLinkLocator);
    }

    public void navigateToCookies() {
        clickEvent(cookiesLocator);
    }

    public void navigateToAccessibilityStatement() {
        clickEvent(accessibilityStatementLocator);
    }

    public void navigateToHelp () {
        clickEvent(helpLocator);
    }

    public void navigateToContactUs () {
        clickEvent(contactUsLocator);
    }

    public void navigateToTermsConditions () {
        clickEvent(termsConditionsLocator);
    }

    public void navigateToPrivacyNotice () {
        clickEvent(privacyNoticeLocator);
    }

    public void navigateToCopyrightLogo () {
        clickEvent(copyrightLogoLocator);
    }

    public void navigateToOpenLicence () {
        clickEvent(openLicenceLocator);
    }

    public String getFooterLinksPageTitle(){
        List<String> browserTabs = new ArrayList<String>(driver.getWindowHandles());
        driver.switchTo().window(browserTabs.get(1));
        String getFooterLinksPageTitle = getPageTitles();
        driver.close();
        driver.switchTo().window(browserTabs.get(0));
        return getFooterLinksPageTitle;
    }

    public String getFooterLinksPageURL(){
        List<String> browserTabs = new ArrayList<String>(driver.getWindowHandles());
        driver.switchTo().window(browserTabs.get(1));
        String getFooterLinksPageURL = getUrl();
        driver.close();
        driver.switchTo().window(browserTabs.get(0));
        return getFooterLinksPageURL;
    }

    public String getCurrentPageTitle(){
        String getCurrentPageTitle = getPageTitles();
        return getCurrentPageTitle;
    }

    public String getCurrentURL() {
        String getCurrentURL = getUrl();
        return getCurrentURL;
    }

    public String getReferenceNumber(){
        String getReferenceNumber = getElementText(referenceLocator);
        System.out.println(getReferenceNumber);
        return getReferenceNumber;
    }
}
