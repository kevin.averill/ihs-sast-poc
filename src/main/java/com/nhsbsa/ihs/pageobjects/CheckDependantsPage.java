package com.nhsbsa.ihs.pageobjects;

import org.openqa.selenium.By;
import org.openqa.selenium.WebDriver;

public class CheckDependantsPage extends Page {

    private By deleteDependantLocator = By.id("delete-dependant-1");
    private By yesRadioButtonLocator = By.id("add-more-dependants-yes");
    private By noRadioButtonLocator = By.id("add-more-dependants-no");
    private By continueButtonLocator = By.id("continue-button");
    private By deleteSuccessLocator = By.id("dependant-delete-success");
    private By errorMessageLocator = By.partialLinkText("Select 'Yes' if");
    private By changeLinkDependantNameLocator = By.id("dependant-name-1-change");
    private By changeLinkDependantDOBLocator = By.id("dependant-date-of-birth-1-change");
    private By changeLinkDependantIHSNumberLocator = By.id("dependant-immigration-health-surcharge-number-1-change");
    private By changeLinkDependantShareCodeLocator = By.id("dependant-share-code-1-change");
    private By changeLinkDependantAddressLocator = By.id("dependant-enter-address-1-change");
    private By displayedNameLocator = By.id("dependant-name-1-value");
    private By displayedDOBLocator = By.id("dependant-date-of-birth-1-value");
    private By displayedIHSNumberLocator = By.id("dependant-immigration-health-surcharge-number-1-value");
    private By displayedShareCodeLocator = By.id("dependant-share-code-1-value");
    private By displayedAddressLocator = By.id("dependant-enter-address-1-value");

    public CheckDependantsPage(WebDriver driver) {
        super(driver);
    }

    public void continueButton() {
        clickEvent(continueButtonLocator);
    }

    public void selectYesRadioButtonAndClickContinue() {
        clickEvent(yesRadioButtonLocator);
        continueButton();
    }

    public void selectNoRadioButtonAndClickContinue() {
        clickEvent(noRadioButtonLocator);
        continueButton();
    }

    public void deleteDependantLink() {
        clickEvent(deleteDependantLocator);
    }

    public void changeDependantName() {
        clickEvent(changeLinkDependantNameLocator);
    }

    public void changeDependantDOB() {
        clickEvent(changeLinkDependantDOBLocator);
    }

    public void changeDependantIHSNumber() {
        clickEvent(changeLinkDependantIHSNumberLocator);
    }

    public void changeDependantShareCode() {
        clickEvent(changeLinkDependantShareCodeLocator);
    }

    public void changeDependantAddress() {
        clickEvent(changeLinkDependantAddressLocator);
    }

    public String getDisplayedName() {
        return getElementText(displayedNameLocator);
    }

    public String getDisplayedDoB() {
        return getElementText(displayedDOBLocator);
    }

    public String getDisplayedIHSNumber() {
        return getElementText(displayedIHSNumberLocator);
    }

    public String getDisplayedShareCode() {
        return getElementText(displayedShareCodeLocator);
    }

    public String getDisplayedAddress() {
        return getElementText(displayedAddressLocator);
    }

    public String getErrorMessageSelectNoneOption() {
        String getErrorMessageSelectNoneOption = getElementText(errorMessageLocator);
        return getErrorMessageSelectNoneOption;
    }

    public String getDeleteSuccessMessage() {
        String deleteSuccessMessage = getElementText(deleteSuccessLocator);
        return deleteSuccessMessage;
    }
}
