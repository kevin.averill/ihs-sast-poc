package com.nhsbsa.ihs.pageobjects;

import org.openqa.selenium.By;
import org.openqa.selenium.WebDriver;

public class DateOfBirthPage extends Page {

    private By dayLocator = By.id("date-of-birth-day");
    private By monthLocator = By.id("date-of-birth-month");
    private By yearLocator = By.id("date-of-birth-year");
    private By continueButtonLocator = By.id("continue-button");
    private By dateOfBirthErrorMessageLocator = By.partialLinkText("Enter the");
    private By minimumYearsErrorMessageLocator = By.partialLinkText("Enter a");

    public DateOfBirthPage(WebDriver driver) {
        super(driver);
    }

    public void continueButton() {
        clickEvent(continueButtonLocator);
    }

    public void enterDateOfBirthAndSubmit(String day, String month, String year) {
        sendTextValues(dayLocator, day);
        sendTextValues(monthLocator, month);
        sendTextValues(yearLocator, year);
        continueButton();
    }

    public String getDOBErrorMessage() {
        String getDOBErrorMessage = getElementText(dateOfBirthErrorMessageLocator);
        return getDOBErrorMessage;
    }

    public String getMinimumYearsErrorMessage() {
        String getMinimumYearsErrorMessage = getElementText(minimumYearsErrorMessageLocator);
        return getMinimumYearsErrorMessage;
    }

    public String getEnteredDay() {
        return getElementValue(dayLocator);
    }

    public String getEnteredMonth() {
        return getElementValue(monthLocator);
    }

    public String getEnteredYear() {
        return getElementValue(yearLocator);
    }

    public void enterDateOfBirth(String day, String month, String year) {
        sendTextValues(dayLocator, day);
        sendTextValues(monthLocator, month);
        sendTextValues(yearLocator, year);
    }


}