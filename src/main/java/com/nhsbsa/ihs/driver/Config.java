package com.nhsbsa.ihs.driver;

import com.nhsbsa.ihs.shared.SharedData;
import io.github.bonigarcia.wdm.WebDriverManager;
import org.openqa.selenium.Proxy;
import org.openqa.selenium.WebDriver;
import org.openqa.selenium.chrome.ChromeDriver;
import org.openqa.selenium.chrome.ChromeOptions;
import org.openqa.selenium.edge.EdgeDriver;
import org.openqa.selenium.firefox.FirefoxDriver;
import org.openqa.selenium.ie.InternetExplorerDriver;
import org.openqa.selenium.remote.CapabilityType;
import org.openqa.selenium.remote.DesiredCapabilities;

import java.time.Duration;
import java.util.*;

public class Config {

    private static WebDriver driver;

    public static WebDriver setDriver() {
        SharedData.browser = System.getProperty("browser");
        SharedData.environment = System.getProperty("env");

        switch (SharedData.browser) {
            case "chrome":
                driver = chromeDriver();
                break;
            case "firefox":
                driver = firefoxDriver();
                break;
            case "headless":
                driver = headlessDriver();
                break;
            case "edge":
                driver = edgeDriver();
                break;
            case "ie":
                driver = ieDriver();
                break;
        }
        return driver;
    }

    public static WebDriver getDriver() {
        return driver;
    }

    private static WebDriver headlessDriver() {
        String proxyAddress = System.getProperty("proxy");
        WebDriverManager.chromedriver().setup();
        ChromeOptions options = new ChromeOptions();
        options.addArguments("--no-sandbox");
        options.addArguments("--headless");
        options.addArguments("--disable-gpu");
        options.addArguments("--disable-extensions");
        options.addArguments("--disable-dev-shm-usage");
        if(proxyAddress != null) {
            Proxy proxy = new Proxy();
            proxy.setAutodetect(false);
            proxy.setHttpProxy(proxyAddress);
            proxy.setSslProxy(proxyAddress);
            proxy.setNoProxy("localhost");
            System.out.println("Using proxy for http and ssl: " + proxyAddress);
            options.setCapability("proxy", proxy);
        }
        WebDriver driver = new ChromeDriver(options);
        configureCommonSettings(driver);
        return driver;
    }

    private static WebDriver chromeDriver() {
        WebDriverManager.chromedriver().setup();
        WebDriver driver = new ChromeDriver();
        configureCommonSettings(driver);
        return driver;
    }

    private static WebDriver firefoxDriver() {
        WebDriverManager.firefoxdriver().setup();
        WebDriver driver = new FirefoxDriver();
        configureCommonSettings(driver);
        return driver;
    }

    private static WebDriver edgeDriver() {
        WebDriverManager.edgedriver().setup();
        WebDriver driver = new EdgeDriver();
        configureCommonSettings(driver);
        return driver;
    }

    private static WebDriver ieDriver() {
        WebDriverManager.iedriver().setup();
        WebDriver driver = new InternetExplorerDriver();
        configureCommonSettings(driver);
        return driver;
    }

    private static DesiredCapabilities chromeCapabilities() {
        if (SharedData.browser.equals("headless")) {
            ArrayList<String> capabilityFlags = new ArrayList<>();
            capabilityFlags.add("--ignore-certificate-errors=true");
            DesiredCapabilities capabilities = new DesiredCapabilities();
            capabilities.setCapability("chrome.switches", capabilityFlags);
            capabilities.setCapability(CapabilityType.ACCEPT_SSL_CERTS, true);
            return capabilities;
        } else {
            String downloadFilepath = "target";
            HashMap<String, Object> chromePrefs = new HashMap<>();
            chromePrefs.put("profile.default_content_settings.popups", 0);
            chromePrefs.put("download.default_directory", downloadFilepath);
            ChromeOptions options = new ChromeOptions();
            options.setExperimentalOption("prefs", chromePrefs);
            options.addArguments("window-size=1024,1080");
            options.addArguments("--start-maximized");
            ArrayList<String> capabilityFlags = new ArrayList<>();
            capabilityFlags.add("--ignore-certificate-errors=true");
            DesiredCapabilities capabilities = new DesiredCapabilities();
            capabilities.setCapability("chrome.switches", capabilityFlags);
            capabilities.setCapability(CapabilityType.ACCEPT_SSL_CERTS, true);
            capabilities.setCapability(ChromeOptions.CAPABILITY, options);
            return capabilities;
        }
    }

    private static DesiredCapabilities firefoxCapabilities() {
        DesiredCapabilities capabilities = new DesiredCapabilities();
        capabilities.setCapability("acceptInsecureCerts", true);
        return capabilities;
    }

    private static void configureCommonSettings(WebDriver driver) {
        driver.manage().timeouts().pageLoadTimeout(Duration.ofSeconds(50));
        driver.manage().window().maximize();
    }
}