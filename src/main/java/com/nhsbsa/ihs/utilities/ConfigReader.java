package com.nhsbsa.ihs.utilities;

import com.nhsbsa.ihs.shared.SharedData;

public class ConfigReader {

    private static String baseURL;
    private static String getURL;

    public static String getEnvironment() {
        SharedData.environment = System.getProperty("env");
        getURL = System.getenv("IHS_STUDENT_CLAIM");

        switch (SharedData.environment) {
            case "dev": case "test": case "stage":
                baseURL = getURL.replace("{env}" , SharedData.environment);
                break;
            case "prod":
                baseURL = (System.getenv("IHS_STUDENT_PROD"));
                break;
            case "local":
                baseURL = (System.getenv("IHS_STUDENT_LOCAL"));
                break;
        }
        return baseURL;
    }
}
