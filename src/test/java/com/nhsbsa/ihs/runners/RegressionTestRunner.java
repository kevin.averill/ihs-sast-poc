package com.nhsbsa.ihs.runners;

import com.nhsbsa.ihs.driver.Config;
import com.nhsbsa.ihs.pageobjects.Page;
import io.cucumber.junit.Cucumber;
import io.cucumber.junit.CucumberOptions;
import org.junit.AfterClass;
import org.junit.BeforeClass;
import org.junit.runner.RunWith;
import org.openqa.selenium.WebDriver;

@RunWith(Cucumber.class)
@CucumberOptions(
        glue = {"com.nhsbsa.ihs.stepdefs"},
        features = {"src/test/resources/features/"},
        plugin = {"com.aventstack.extentreports.cucumber.adapter.ExtentCucumberAdapter:" , "pretty" , "json:target/cucumber-json/cucumber.json"},
        tags = "@Regression"
)

public class RegressionTestRunner {

    private static WebDriver driver;

    @BeforeClass
    public static void setup() {
        driver = Config.setDriver();
    }

    @AfterClass
    public static void teardown() {
        Page page = new Page(driver);
        page.tearDownDriver();
    }
}