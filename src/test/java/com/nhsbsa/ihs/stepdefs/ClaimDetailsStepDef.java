package com.nhsbsa.ihs.stepdefs;

import com.nhsbsa.ihs.driver.Config;
import com.nhsbsa.ihs.pageobjects.*;
import io.cucumber.java.en.And;
import io.cucumber.java.en.When;
import io.cucumber.java.en.Then;
import org.junit.Assert;
import org.openqa.selenium.WebDriver;

import static com.nhsbsa.ihs.stepdefs.Constants.*;

public class ClaimDetailsStepDef {
    private WebDriver driver;
    private CommonPage commonPage;
    private NamePage namePage;
    private DateOfBirthPage dateOfBirthPage;
    private IHSNumberPage ihsNumberPage;
    private VisaShareCodePage visaShareCodePage;
    private EnterAddressPage enterAddressPage;
    private EmailPage emailPage;
    private MobileNumberPage mobileNumberPage;

    public ClaimDetailsStepDef() {
        driver = Config.getDriver();
        commonPage = new CommonPage(driver);
        namePage = new NamePage(driver);
        dateOfBirthPage = new DateOfBirthPage(driver);
        ihsNumberPage = new IHSNumberPage(driver);
        visaShareCodePage = new VisaShareCodePage(driver);
        emailPage = new EmailPage(driver);
        mobileNumberPage = new MobileNumberPage(driver);
        enterAddressPage = new EnterAddressPage(driver);
    }

    @And("^My firstname is (.*) and surname is (.*)$")
    public void myFirstnameIsGivenNameAndSurnameIsFamilyName(String GivenName, String FamilyName) {
        namePage.enterNameAndSubmit(GivenName, FamilyName);
    }

    @When("^I attempt to enter (.*) characters name$")
    public void iAttemptToEnterOverLimitCharactersName(int count) {
        namePage.enterOverLimitText(count);
    }

    @Then("^I am restricted to enter (.*) characters name$")
    public void iAmRestrictedToEnterMaximumCharactersName(int maximum) {
        Assert.assertEquals(namePage.getEnteredTextCount(), maximum);
        namePage.continueButton();
    }

    @And("^My date of birth is (.*) (.*) (.*)$")
    public void myDateOfBirthIsDayMonthYear(String Day, String Month, String Year) {
        dateOfBirthPage.enterDateOfBirthAndSubmit(Day, Month, Year);
    }

    @When("^My IHS number is (.*)$")
    public void myIHSNumberIsIHSNumber(String IHSNumber) {
        ihsNumberPage.enterIHSNumberAndSubmit(IHSNumber);
    }

    @When("^My visa share code is (.*)$")
    public void myVisaShareCodeIsVisaCode(String VisaCode) {
        visaShareCodePage.enterVisaShareCodeAndSubmit(VisaCode);
    }

    @When("^My address is (.*) (.*) (.*) (.*) and (.*)$")
    public void myAddressIsBuildingStreetTownCountyAndPostcode(String Building, String Street, String Town, String County, String Postcode) throws InterruptedException {
        enterAddressPage.enterAddressAndSubmit(Building, Street, Town, County, Postcode);
    }

    @When("^My Email address is (.*)$")
    public void myEmailAddressIsEmail(String Email) {
        emailPage.enterEmailAndSubmit(Email);
    }

    @When("^My Phone number is (.*)$")
    public void myPhoneNumberIsPhoneNumber(String PhoneNumber) {
        mobileNumberPage.enterMobileNumberAndSubmit(PhoneNumber);
    }

    @When("^I verify my answers and confirm$")
    public void iVerifyMyAnswersAndConfirm() {
        commonPage.confirmOnCheckAnswers();
    }

    @And("^I check my answers and submit the claim application$")
    public void iCheckMyAnswersAndSubmitTheClaimApplication() {
        commonPage.confirmOnCheckAnswers();
        commonPage.acceptOnDeclaration();
    }

    @Then("^My claim is submitted successfully$")
    public void myClaimIsSubmittedSuccessfully() {
        Assert.assertEquals(commonPage.getCurrentPageTitle(), CONFIRMATION_PAGE_TITLE);
        Assert.assertTrue(commonPage.getReferenceNumber().contains("BSA"));
    }
}

