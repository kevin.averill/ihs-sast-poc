package com.nhsbsa.ihs.stepdefs;

import com.nhsbsa.ihs.driver.Config;
import com.nhsbsa.ihs.pageobjects.CheckDependantsPage;
import com.nhsbsa.ihs.pageobjects.CommonPage;
import com.nhsbsa.ihs.pageobjects.DependantNotEligiblePage;
import com.nhsbsa.ihs.pageobjects.VisaShareCodePage;
import io.cucumber.java.en.When;
import org.openqa.selenium.WebDriver;

public class CommonStepDef {

    private WebDriver driver;
    private CommonPage commonPage;
    private DependantNotEligiblePage dependantNotEligiblePage;
    private VisaShareCodePage visaShareCodePage;
    private CheckDependantsPage checkDependantsPage;

    public CommonStepDef() {
        driver = Config.getDriver();
        commonPage = new CommonPage(driver);
        dependantNotEligiblePage = new DependantNotEligiblePage(driver);
        visaShareCodePage = new VisaShareCodePage(driver);
        checkDependantsPage = new CheckDependantsPage(driver);
    }

    @When("^I select the (.*)$")
    public void iSelectTheHyperlink(String hyperlink) {
        switch (hyperlink) {
            case "Service Name link":
                commonPage.serviceNameLink();
                break;
            case "Back link":
                commonPage.backLink();
                break;
            case "GOV.UK link":
                commonPage.govUKLink();
                break;
            case "Help link":
                commonPage.navigateToHelp();
                break;
            case "Cookies link":
                commonPage.navigateToCookies();
                break;
            case "Accessibility Statement link":
                commonPage.navigateToAccessibilityStatement();
                break;
            case "Contact link":
                commonPage.navigateToContactUs();
                break;
            case "Terms and Conditions link":
                commonPage.navigateToTermsConditions();
                break;
            case "Privacy link":
                commonPage.navigateToPrivacyNotice();
                break;
            case "Share code GOV.UK link":
                visaShareCodePage.navigateToShareCode();
                break;
            case "Contact Us link":
                dependantNotEligiblePage.navigateToContactUs();
                break;
            case "More Info link":
                dependantNotEligiblePage.navigateToMoreInfo();
                break;
            case "Delete Dependant link":
                checkDependantsPage.deleteDependantLink();
                break;
            case "Privacy info link":
                commonPage.declarationPrivacy();
                break;
            case "End of service survey link":
                commonPage.navigateToFeedbackForm();
                break;
            case "Confirmation Contact link":
                commonPage.confirmationContact();
        }
    }
}
