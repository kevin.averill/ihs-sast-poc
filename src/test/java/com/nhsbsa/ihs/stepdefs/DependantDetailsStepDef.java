package com.nhsbsa.ihs.stepdefs;

import com.nhsbsa.ihs.driver.Config;
import com.nhsbsa.ihs.pageobjects.*;
import io.cucumber.java.en.And;
import io.cucumber.java.en.When;
import io.cucumber.java.en.Then;
import org.junit.Assert;
import org.openqa.selenium.WebDriver;

import static com.nhsbsa.ihs.stepdefs.Constants.*;

public class DependantDetailsStepDef {
    private WebDriver driver;
    private AddDependantPage addDependantPage;
    private DependantPaidWordPage dependantPaidWordPage;
    private DependantLivingInUKPage dependantLivingInUKPage;
    private DependantNotEligiblePage dependantNotEligiblePage;
    private DependantNamePage dependantNamePage;
    private DependantDateOfBirthPage dependantDateOfBirthPage;
    private DependantIHSNumberPage dependantIHSNumberPage;
    private DependantVisaShareCodePage dependantVisaShareCodePage;
    private DependantSameAddressPage dependantSameAddressPage;
    private DependantEnterAddressPage dependantEnterAddressPage;
    private CheckDependantsPage checkDependantsPage;
    private UploadEHICFilePage uploadEHICFilePage;
    private UploadDependantEHICFilePage uploadDependantEHICFilePage;

    public DependantDetailsStepDef() {
        driver = Config.getDriver();
        addDependantPage = new AddDependantPage(driver);
        dependantPaidWordPage = new DependantPaidWordPage(driver);
        dependantLivingInUKPage = new DependantLivingInUKPage(driver);
        dependantNotEligiblePage = new DependantNotEligiblePage(driver);
        dependantNamePage = new DependantNamePage(driver);
        dependantDateOfBirthPage = new DependantDateOfBirthPage(driver);
        dependantIHSNumberPage = new DependantIHSNumberPage(driver);
        dependantVisaShareCodePage = new DependantVisaShareCodePage(driver);
        dependantSameAddressPage = new DependantSameAddressPage(driver);
        dependantEnterAddressPage = new DependantEnterAddressPage(driver);
        checkDependantsPage = new CheckDependantsPage(driver);
        uploadEHICFilePage = new UploadEHICFilePage(driver);
        uploadDependantEHICFilePage = new UploadDependantEHICFilePage(driver);
    }

    @And("^I (.*) have dependant to add$")
    public void iDependantOptionHaveDependantToAdd(String dependantOption) {
        switch (dependantOption) {
            case "do":
                addDependantPage.selectYesRadioButtonAndClickContinue();
                break;
            case "do not":
                addDependantPage.selectNoRadioButtonAndClickContinue();
                break;
            case "":
                addDependantPage.continueButton();
                break;
        }
    }

    @When("^My dependant (.*) carrying out paid work in UK$")
    public void myDependantWorkOptionCarryingOutPaidWorkInUK(String workOption) {
        switch (workOption) {
            case "is":
                dependantPaidWordPage.selectYesRadioButtonAndClickContinue();
                break;
            case "is not":
                dependantPaidWordPage.selectNoRadioButtonAndClickContinue();
                break;
            case "":
                dependantPaidWordPage.continueButton();
                break;
        }
    }

    @When("^My dependant (.*) currently residing in UK$")
    public void myDependantResidingOptionCurrentlyResidingInUK(String residingOption) {
        switch (residingOption) {
            case "is":
                dependantLivingInUKPage.selectYesRadioButtonAndClickContinue();
                break;
            case "is not":
                dependantLivingInUKPage.selectNoRadioButtonAndClickContinue();
                break;
            case "":
                dependantLivingInUKPage.continueButton();
                break;
        }
    }

    @When("^My dependant's firstname is (.*) and surname is (.*)$")
    public void myDependantSFirstnameIsGivenNameAndSurnameIsFamilyName(String GivenName, String FamilyName) {
        dependantNamePage.enterNameAndSubmit(GivenName, FamilyName);
    }

    @When("^I attempt to enter (.*) characters dependant's name$")
    public void iAttemptToEnterCountCharactersDependantSName(int count) {
        dependantNamePage.enterOverLimitText(count);
    }

    @Then("^I am restricted to enter (.*) characters dependant's name$")
    public void iAmRestrictedToEnterMaximumCharactersDependantSName(int maximum) {
        Assert.assertEquals(dependantNamePage.getEnteredTextCount(), maximum);
        dependantNamePage.continueButton();
    }

    @When("^My dependant's date of birth is (.*) (.*) (.*)$")
    public void myDependantSDateOfBirthIsDayMonthYear(String Day, String Month, String Year) {
        dependantDateOfBirthPage.enterDateOfBirthAndSubmit(Day, Month, Year);
    }

    @When("^My dependant's IHS number is (.*)$")
    public void myDependantSIHSNumberIsIHSNumber(String IHSNumber) {
        dependantIHSNumberPage.enterIHSNumberAndSubmit(IHSNumber);
    }

    @When("^My dependant's visa share code is (.*)$")
    public void myDependantSVisaShareCodeIsVisaCode(String VisaCode) {
        dependantVisaShareCodePage.enterVisaShareCodeAndSubmit(VisaCode);
    }

    @When("^My dependant (.*) live in the same UK address$")
    public void myDependantAddressOptionLiveInTheSameUKAddress(String addressOption) {
        switch (addressOption) {
            case "does":
                dependantSameAddressPage.selectYesRadioButtonAndClickContinue();
                break;
            case "does not":
                dependantSameAddressPage.selectNoRadioButtonAndClickContinue();
                break;
            case "":
                dependantSameAddressPage.continueButton();
                break;
        }
    }

    @When("^My dependant's address is (.*) (.*) (.*) (.*) and (.*)$")
    public void myDependantSAddressIsBuildingStreetTownCountyAndPostcode(String Building, String Street, String Town, String County, String Postcode) {
        dependantEnterAddressPage.enterAddressAndSubmit(Building, Street, Town, County, Postcode);
    }

    @When("^I (.*) have another dependant to add$")
    public void iDependantMoreOptionHaveAnotherDependantToAdd(String dependantMoreOption) {
        switch (dependantMoreOption) {
            case "do":
                checkDependantsPage.selectYesRadioButtonAndClickContinue();
                break;
            case "do not":
                checkDependantsPage.selectNoRadioButtonAndClickContinue();
                break;
            case "":
                checkDependantsPage.continueButton();
                break;
        }
    }

    @And("^I add (.*) dependants to my claim application$")
    public void iAddDependantsToMyClaimApplication(int count) {
        for (int i = 0; i < (count-1); i++) {
            dependantPaidWordPage.selectNoRadioButtonAndClickContinue();
            dependantLivingInUKPage.selectYesRadioButtonAndClickContinue();
            dependantNamePage.enterNameAndSubmit(GIVEN_NAME, FAMILY_NAME);
            dependantDateOfBirthPage.enterDateOfBirthAndSubmit(DOB_DAY, DOB_MONTH, DOB_YEAR);
            dependantIHSNumberPage.enterIHSNumberAndSubmit(IHS_NUMBER);
            dependantVisaShareCodePage.enterVisaShareCodeAndSubmit(VISA_SHARE_CODE);
            dependantSameAddressPage.selectYesRadioButtonAndClickContinue();
            uploadDependantEHICFilePage.chooseDependantEHICAndContinue(FILE_UPLOAD_FILEPATH + "Payslip-June" + ".png");
            uploadEHICFilePage.continueOnViewEHIC();
            checkDependantsPage.selectYesRadioButtonAndClickContinue();
        }
        dependantPaidWordPage.selectNoRadioButtonAndClickContinue();
        dependantLivingInUKPage.selectYesRadioButtonAndClickContinue();
        dependantNamePage.enterNameAndSubmit(GIVEN_NAME, FAMILY_NAME);
        dependantDateOfBirthPage.enterDateOfBirthAndSubmit(DOB_DAY, DOB_MONTH, DOB_YEAR);
        dependantIHSNumberPage.enterIHSNumberAndSubmit(IHS_NUMBER);
        dependantVisaShareCodePage.enterVisaShareCodeAndSubmit(VISA_SHARE_CODE);
        dependantSameAddressPage.selectYesRadioButtonAndClickContinue();
        uploadDependantEHICFilePage.chooseDependantEHICAndContinue(FILE_UPLOAD_FILEPATH + "Payslip-June" + ".png");
        uploadEHICFilePage.continueOnViewEHIC();
        checkDependantsPage.selectNoRadioButtonAndClickContinue();
    }

    @When("^I add a dependant who holds valid EHIC$")
    public void iAddADependantWhoHoldsValidEHIC() {
        addDependantPage.selectYesRadioButtonAndClickContinue();
        dependantPaidWordPage.selectNoRadioButtonAndClickContinue();
        dependantLivingInUKPage.selectYesRadioButtonAndClickContinue();
        dependantNamePage.enterNameAndSubmit(DEPENDANT_GIVEN_NAME,DEPENDANT_FAMILY_NAME);
        dependantDateOfBirthPage.enterDateOfBirthAndSubmit(DEPENDANT_DOB_DAY,DEPENDANT_DOB_MONTH,DEPENDANT_DOB_YEAR);
        dependantIHSNumberPage.enterIHSNumberAndSubmit(DEPENDANT_IHS_NUMBER);
        dependantVisaShareCodePage.enterVisaShareCodeAndSubmit(DEPENDANT_VISA_SHARE_CODE);
        dependantSameAddressPage.selectNoRadioButtonAndClickContinue();
        dependantEnterAddressPage.enterAddressAndSubmit(DEPENDANT_BUILDING_NUMBER, DEPENDANT_STREET_NAME, DEPENDANT_TOWN_NAME, DEPENDANT_COUNTY_NAME, DEPENDANT_POST_CODE);
        uploadDependantEHICFilePage.chooseDependantEHICAndContinue(FILE_UPLOAD_FILEPATH + "123456" + ".pdf");
        uploadDependantEHICFilePage.continueOnViewEHIC();
    }

    @When("I add a dependant with same UK address")
    public void iAddADependantWithSameUKAddress() {
        addDependantPage.selectYesRadioButtonAndClickContinue();
        dependantPaidWordPage.selectNoRadioButtonAndClickContinue();
        dependantLivingInUKPage.selectYesRadioButtonAndClickContinue();
        dependantNamePage.enterNameAndSubmit(DEPENDANT_GIVEN_NAME,DEPENDANT_FAMILY_NAME);
        dependantDateOfBirthPage.enterDateOfBirthAndSubmit(DEPENDANT_DOB_DAY,DEPENDANT_DOB_MONTH,DEPENDANT_DOB_YEAR);
        dependantIHSNumberPage.enterIHSNumberAndSubmit(DEPENDANT_IHS_NUMBER);
        dependantVisaShareCodePage.enterVisaShareCodeAndSubmit(DEPENDANT_VISA_SHARE_CODE);
        dependantSameAddressPage.selectYesRadioButtonAndClickContinue();
        uploadDependantEHICFilePage.chooseDependantEHICAndContinue(FILE_UPLOAD_FILEPATH + "123456" + ".pdf");
        uploadDependantEHICFilePage.continueOnViewEHIC();
    }

    @When("I add a dependant with different UK address")
    public void iAddADependantWithDifferentUKAddress() {
        addDependantPage.selectYesRadioButtonAndClickContinue();
        dependantPaidWordPage.selectNoRadioButtonAndClickContinue();
        dependantLivingInUKPage.selectYesRadioButtonAndClickContinue();
        dependantNamePage.enterNameAndSubmit(DEPENDANT_GIVEN_NAME,DEPENDANT_FAMILY_NAME);
        dependantDateOfBirthPage.enterDateOfBirthAndSubmit(DEPENDANT_DOB_DAY,DEPENDANT_DOB_MONTH,DEPENDANT_DOB_YEAR);
        dependantIHSNumberPage.enterIHSNumberAndSubmit(DEPENDANT_IHS_NUMBER);
        dependantVisaShareCodePage.enterVisaShareCodeAndSubmit(DEPENDANT_VISA_SHARE_CODE);
        dependantSameAddressPage.selectNoRadioButtonAndClickContinue();
        dependantEnterAddressPage.enterAddressAndSubmit(DEPENDANT_BUILDING_NUMBER, DEPENDANT_STREET_NAME, DEPENDANT_TOWN_NAME, DEPENDANT_COUNTY_NAME, DEPENDANT_POST_CODE);
        uploadDependantEHICFilePage.chooseDependantEHICAndContinue(FILE_UPLOAD_FILEPATH + "123456" + ".pdf");
        uploadDependantEHICFilePage.continueOnViewEHIC();
    }

    @And("^I select (.*) in Check Dependants screen$")
    public void iSelectChangeLinkInCheckDependantsScreen(String changeLink) {
        switch (changeLink) {
            case "Change Name link":
                checkDependantsPage.changeDependantName();
                break;
            case "Change Date of Birth link":
                checkDependantsPage.changeDependantDOB();
                break;
            case "Change IHS Number link":
                checkDependantsPage.changeDependantIHSNumber();
                break;
            case "Change Visa Share Code link":
                checkDependantsPage.changeDependantShareCode();
                break;
            case "Change Address link":
                checkDependantsPage.changeDependantAddress();
                break;
        }
    }

    @And("^my dependant's (.*) is pre-populated$")
    public void myDependantSAnswerIsPrePopulated(String answer) {
        switch (answer) {
            case "name":
                Assert.assertEquals(dependantNamePage.getEnteredGivenName(), DEPENDANT_GIVEN_NAME);
                Assert.assertEquals(dependantNamePage.getEnteredFamilyName(), DEPENDANT_FAMILY_NAME);
                break;
            case "date of birth":
                Assert.assertEquals(dependantDateOfBirthPage.getEnteredDay(), DEPENDANT_DOB_DAY);
                Assert.assertEquals(dependantDateOfBirthPage.getEnteredMonth(), DEPENDANT_DOB_MONTH);
                Assert.assertEquals(dependantDateOfBirthPage.getEnteredYear(), DEPENDANT_DOB_YEAR);
                break;
            case "IHS number":
                Assert.assertEquals(dependantIHSNumberPage.getEnteredIHSNumber(), DEPENDANT_IHS_NUMBER);
                break;
            case "visa share code":
                Assert.assertEquals(dependantVisaShareCodePage.getEnteredShareCode(), DEPENDANT_VISA_SHARE_CODE);
                break;
            case "address":
                Assert.assertEquals(dependantEnterAddressPage.getEnteredBuilding(), DEPENDANT_BUILDING_NUMBER);
                Assert.assertEquals(dependantEnterAddressPage.getEnteredStreet(), DEPENDANT_STREET_NAME);
                Assert.assertEquals(dependantEnterAddressPage.getEnteredTown(), DEPENDANT_TOWN_NAME);
                Assert.assertEquals(dependantEnterAddressPage.getEnteredCounty(), DEPENDANT_COUNTY_NAME);
                Assert.assertEquals(dependantEnterAddressPage.getEnteredPostCode(), DEPENDANT_POST_CODE);
                break;
        }
    }

    @And("^my applicant's (.*) is pre-populated$")
    public void myApplicantSParameterIsPrePopulated(String parameter) {
        Assert.assertEquals(dependantEnterAddressPage.getEnteredBuilding(), BUILDING_NUMBER);
        Assert.assertEquals(dependantEnterAddressPage.getEnteredStreet(), STREET_NAME);
        Assert.assertEquals(dependantEnterAddressPage.getEnteredTown(), TOWN_NAME);
        Assert.assertEquals(dependantEnterAddressPage.getEnteredCounty(), COUNTY_NAME);
        Assert.assertEquals(dependantEnterAddressPage.getEnteredPostCode(), POST_CODE);
    }

    @When("^I do not change the details and continue$")
    public void iDoNotChangeTheDetailsAndContinue() {
        checkDependantsPage.continueButton();
    }

    @And("^my dependant's (.*) is not changed$")
    public void myDependantSParameterIsNotChanged(String parameter) {
        switch (parameter) {
            case "name":
                Assert.assertEquals(checkDependantsPage.getDisplayedName(), DEPENDANT_GIVEN_NAME + " " + DEPENDANT_FAMILY_NAME);
                break;
            case "date of birth":
                Assert.assertEquals(checkDependantsPage.getDisplayedDoB(), DEPENDANT_DOB_DAY + " " + DEPENDANT_DOB_FULL_MONTH + " " + DEPENDANT_DOB_YEAR);
                break;
            case "IHS number":
                Assert.assertEquals(checkDependantsPage.getDisplayedIHSNumber(), DEPENDANT_IHS_NUMBER);
                break;
            case "visa share code":
                Assert.assertEquals(checkDependantsPage.getDisplayedShareCode(), DEPENDANT_VISA_SHARE_CODE);
                break;
            case "address":
                Assert.assertEquals(checkDependantsPage.getDisplayedAddress(), DEPENDANT_BUILDING_NUMBER + "\n" +
                        DEPENDANT_STREET_NAME + "\n" +
                        DEPENDANT_TOWN_NAME + "\n" +
                        DEPENDANT_COUNTY_NAME + "\n" +
                        DEPENDANT_POST_CODE);
                break;
        }
    }

    @When("^I change the dependant's (.*) and continue$")
    public void iChangeTheDependantSAnswerAndContinue(String answer) {
        switch (answer) {
            case "name":
                dependantNamePage.enterNameAndSubmit(UPDATED_DEPENDANT_GIVEN_NAME,UPDATED_DEPENDANT_FAMILY_NAME);
                break;
            case "date of birth":
                dependantDateOfBirthPage.enterDateOfBirthAndSubmit(UPDATED_DEPENDANT_DOB_DAY,UPDATED_DEPENDANT_DOB_MONTH,UPDATED_DEPENDANT_DOB_YEAR);
                break;
            case "IHS number":
                dependantIHSNumberPage.enterIHSNumberAndSubmit(UPDATED_DEPENDANT_IHS_NUMBER);
                break;
            case "visa share code":
                dependantVisaShareCodePage.enterVisaShareCodeAndSubmit(UPDATED_DEPENDANT_VISA_SHARE_CODE);
                break;
            case "address":
                dependantEnterAddressPage.enterAddressAndSubmit(UPDATED_DEPENDANT_BUILDING_NUMBER, UPDATED_DEPENDANT_STREET_NAME, UPDATED_DEPENDANT_TOWN_NAME, UPDATED_DEPENDANT_COUNTY_NAME, UPDATED_DEPENDANT_POST_CODE);
                break;
        }
    }

    @When("^I change the dependant's answer as (.*)$")
    public void iChangeTheDependantSAnswerAsInvalid(String invalid) {
        switch (invalid) {
            case "blank name":
                dependantNamePage.enterNameAndSubmit("",UPDATED_DEPENDANT_FAMILY_NAME);
                break;
            case "invalid name":
                dependantNamePage.enterNameAndSubmit(UPDATED_DEPENDANT_GIVEN_NAME,UPDATED_DEPENDANT_FAMILY_NAME+"123");
                break;
            case "blank date of birth":
                dependantDateOfBirthPage.enterDateOfBirthAndSubmit("","","");
                break;
            case "invalid date of birth":
                dependantDateOfBirthPage.enterDateOfBirthAndSubmit("dd","mm","yyyy");
                break;
            case "blank IHS number":
                dependantIHSNumberPage.enterIHSNumberAndSubmit("");
                break;
            case "invalid IHS number":
                dependantIHSNumberPage.enterIHSNumberAndSubmit("asvboiubg");
                break;
            case "blank visa share code":
                dependantVisaShareCodePage.enterVisaShareCodeAndSubmit("");
                break;
            case "invalid visa share code":
                dependantVisaShareCodePage.enterVisaShareCodeAndSubmit("A1@34@67Z");
                break;
            case "blank address":
                dependantEnterAddressPage.enterAddressAndSubmit("", "", UPDATED_DEPENDANT_TOWN_NAME, UPDATED_DEPENDANT_COUNTY_NAME, UPDATED_DEPENDANT_POST_CODE);
                break;
            case "invalid address":
                dependantEnterAddressPage.enterAddressAndSubmit(UPDATED_DEPENDANT_BUILDING_NUMBER, UPDATED_DEPENDANT_STREET_NAME, UPDATED_DEPENDANT_TOWN_NAME, "(Wiltshire)", UPDATED_DEPENDANT_POST_CODE);
                break;
        }
    }

    @And("^my dependant's (.*) is changed$")
    public void myDependantSAnswerIsChanged(String answer) {
        switch (answer) {
            case "name":
                Assert.assertEquals(checkDependantsPage.getDisplayedName(), UPDATED_DEPENDANT_GIVEN_NAME + " " + UPDATED_DEPENDANT_FAMILY_NAME);
                break;
            case "date of birth":
                Assert.assertEquals(checkDependantsPage.getDisplayedDoB(), UPDATED_DEPENDANT_DOB_DAY + " " + UPDATED_DEPENDANT_DOB_FULL_MONTH + " " + UPDATED_DEPENDANT_DOB_YEAR);
                break;
            case "IHS number":
                Assert.assertEquals(checkDependantsPage.getDisplayedIHSNumber(), UPDATED_DEPENDANT_IHS_NUMBER);
                break;
            case "visa share code":
                Assert.assertEquals(checkDependantsPage.getDisplayedShareCode(), UPDATED_DEPENDANT_VISA_SHARE_CODE);
                break;
            case "address":
                Assert.assertEquals(checkDependantsPage.getDisplayedAddress(), UPDATED_DEPENDANT_BUILDING_NUMBER + "\n" +
                        UPDATED_DEPENDANT_STREET_NAME + "\n" +
                        UPDATED_DEPENDANT_TOWN_NAME + "\n" +
                        UPDATED_DEPENDANT_COUNTY_NAME + "\n" +
                        UPDATED_DEPENDANT_POST_CODE);
                break;
        }
    }

    @Then("^I will see (.*) radio button selected$")
    public void iWillSeeRadioButtonRadioButtonSelected(String radioButton) {
        switch (radioButton) {
            case "No":
                Assert.assertTrue(dependantSameAddressPage.isNoRadioButtonSelected());
                break;
            case "Yes":
                Assert.assertTrue(dependantSameAddressPage.isYesRadioButtonSelected());
                break;
        }
    }

    @When("^I change my answer to (.*)$")
    public void iChangeMyAnswerToRadioButton(String radioButton) {
        dependantSameAddressPage.selectYesRadioButtonAndClickContinue();
    }

    @And("^my dependant's address is (.*) as applicant's address$")
    public void myDependantSAddressIsUpdatedAsApplicantSAddress(String change) {
        Assert.assertEquals(checkDependantsPage.getDisplayedAddress(), BUILDING_NUMBER + "\n" +
                STREET_NAME + "\n" +
                TOWN_NAME + "\n" +
                COUNTY_NAME + "\n" +
                POST_CODE);
    }
}

