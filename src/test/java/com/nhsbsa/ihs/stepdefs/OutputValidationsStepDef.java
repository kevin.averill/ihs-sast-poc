package com.nhsbsa.ihs.stepdefs;

import com.nhsbsa.ihs.driver.Config;
import com.nhsbsa.ihs.pageobjects.*;
import io.cucumber.java.en.Then;
import org.junit.Assert;
import org.openqa.selenium.WebDriver;

import static com.nhsbsa.ihs.stepdefs.Constants.*;
import static com.nhsbsa.ihs.stepdefs.Constants.GOV_UK_FEEDBACK_PAGE;

public class OutputValidationsStepDef {

    private WebDriver driver;
    private CommonPage commonPage;
    private NamePage namePage;
    private DateOfBirthPage dateOfBirthPage;
    private IHSNumberPage ihsNumberPage;
    private VisaShareCodePage visaShareCodePage;
    private EnterAddressPage enterAddressPage;
    private EmailPage emailPage;
    private MobileNumberPage mobileNumberPage;
    private UploadEHICFilePage uploadEHICFilePage;
    private UploadCASFilePage uploadCASFilePage;
    private AddDependantPage addDependantPage;
    private DependantPaidWordPage dependantPaidWordPage;
    private DependantLivingInUKPage dependantLivingInUKPage;
    private DependantNotEligiblePage dependantNotEligiblePage;
    private DependantNamePage dependantNamePage;
    private DependantDateOfBirthPage dependantDateOfBirthPage;
    private DependantIHSNumberPage dependantIHSNumberPage;
    private DependantVisaShareCodePage dependantVisaShareCodePage;
    private DependantSameAddressPage dependantSameAddressPage;
    private DependantEnterAddressPage dependantEnterAddressPage;
    private CheckDependantsPage checkDependantsPage;

    public OutputValidationsStepDef() {
        driver = Config.getDriver();
        commonPage = new CommonPage(driver);
        namePage = new NamePage(driver);
        dateOfBirthPage = new DateOfBirthPage(driver);
        ihsNumberPage = new IHSNumberPage(driver);
        visaShareCodePage = new VisaShareCodePage(driver);
        enterAddressPage = new EnterAddressPage(driver);
        emailPage = new EmailPage(driver);
        mobileNumberPage = new MobileNumberPage(driver);
        uploadEHICFilePage = new UploadEHICFilePage(driver);
        uploadCASFilePage = new UploadCASFilePage(driver);
        addDependantPage = new AddDependantPage(driver);
        dependantPaidWordPage = new DependantPaidWordPage(driver);
        dependantLivingInUKPage = new DependantLivingInUKPage(driver);
        dependantNotEligiblePage = new DependantNotEligiblePage(driver);
        dependantNamePage = new DependantNamePage(driver);
        dependantDateOfBirthPage = new DependantDateOfBirthPage(driver);
        dependantIHSNumberPage = new DependantIHSNumberPage(driver);
        dependantVisaShareCodePage = new DependantVisaShareCodePage(driver);
        dependantSameAddressPage = new DependantSameAddressPage(driver);
        dependantEnterAddressPage = new DependantEnterAddressPage(driver);
        checkDependantsPage = new CheckDependantsPage(driver);
    }

    @Then("^I will see the (.*)$")
    public void iWillSeeTheOutput(String output) {
        switch (output) {
            case "Start screen":
                Assert.assertEquals(commonPage.getCurrentPageTitle(), START_PAGE_TITLE);
                Assert.assertTrue(commonPage.getCurrentURL().contains( START_PAGE_URL));
                break;
            case "IHS Paid screen":
                Assert.assertEquals(commonPage.getCurrentPageTitle(), IHS_PAID_PAGE_TITLE);
                Assert.assertTrue(commonPage.getCurrentURL().contains(IHS_PAID_PAGE_URL));
                break;
            case "Name screen":
                Assert.assertEquals(commonPage.getCurrentPageTitle(), NAME_PAGE_TITLE);
                Assert.assertTrue(commonPage.getCurrentURL().contains(NAME_PAGE_URL));
                break;
            case "Date of birth screen":
                Assert.assertEquals(commonPage.getCurrentPageTitle(), DOB_PAGE_TITLE);
                Assert.assertTrue(commonPage.getCurrentURL().contains(DOB_PAGE_URL));
                break;
            case "IHS Number screen":
                Assert.assertEquals(commonPage.getCurrentPageTitle(), IHS_NUMBER_PAGE_TITLE);
                Assert.assertTrue(commonPage.getCurrentURL().contains(IHS_NUMBER_PAGE_URL));
                break;
            case "Visa Share Code screen":
                Assert.assertEquals(commonPage.getCurrentPageTitle(), VISA_SHARE_CODE_PAGE_TITLE);
                Assert.assertTrue(commonPage.getCurrentURL().contains(VISA_SHARE_CODE_PAGE_URL));
                break;
            case "Enter Address screen":
                Assert.assertEquals(commonPage.getCurrentPageTitle(), ENTER_ADDRESS_PAGE_TITLE);
                Assert.assertTrue(commonPage.getCurrentURL().contains(ENTER_ADDRESS_PAGE_URL));
                break;
            case "Email Address screen":
                Assert.assertEquals(commonPage.getCurrentPageTitle(), EMAIL_PAGE_TITLE);
                Assert.assertTrue(commonPage.getCurrentURL().contains(EMAIL_PAGE_URL));
                break;
            case "Phone number screen":
                Assert.assertEquals(commonPage.getCurrentPageTitle(), PHONE_NUMBER_PAGE_TITLE);
                Assert.assertTrue(commonPage.getCurrentURL().contains(PHONE_NUMBER_PAGE_URL));
                break;
            case "Upload EHIC Card screen":
                Assert.assertEquals(commonPage.getCurrentPageTitle(), UPLOAD_EHIC_PAGE_TITLE);
                Assert.assertTrue(commonPage.getCurrentURL().contains(UPLOAD_EHIC_PAGE_URL));
                break;
            case "View Uploaded EHIC screen":
                Assert.assertEquals(commonPage.getCurrentPageTitle(), VIEW_EHIC_PAGE_TITLE);
                Assert.assertTrue(commonPage.getCurrentURL().contains(VIEW_EHIC_PAGE_URL));
                break;
            case "Upload CAS Letter screen":
                Assert.assertEquals(commonPage.getCurrentPageTitle(), UPLOAD_CAS_PAGE_TITLE);
                Assert.assertTrue(commonPage.getCurrentURL().contains(UPLOAD_CAS_PAGE_URL));
                break;
            case "View Uploaded Evidence screen":
                Assert.assertEquals(commonPage.getCurrentPageTitle(), VIEW_EVIDENCE_PAGE_TITLE);
                Assert.assertTrue(commonPage.getCurrentURL().contains(VIEW_EVIDENCE_PAGE_URL));
                break;
            case "Add Dependant screen":
                Assert.assertEquals(commonPage.getCurrentPageTitle(), ADD_DEPENDANT_PAGE_TITLE);
                Assert.assertTrue(commonPage.getCurrentURL().contains(ADD_DEPENDANT_PAGE_URL));
                break;
            case "Dependant Not Eligible screen":
                Assert.assertEquals(commonPage.getCurrentPageTitle(), DEPENDANT_NOT_ELIGIBLE_PAGE_TITLE);
                Assert.assertTrue(commonPage.getCurrentURL().contains(DEPENDANT_NOT_ELIGIBLE_PAGE_URL));
                break;
            case "Dependant Paid Work screen":
                Assert.assertEquals(commonPage.getCurrentPageTitle(), DEPENDANT_PAID_WORK_PAGE_TITLE);
                Assert.assertTrue(commonPage.getCurrentURL().contains(DEPENDANT_PAID_WORK_PAGE_URL));
                break;
            case "Dependant Living in UK screen":
                Assert.assertEquals(commonPage.getCurrentPageTitle(), DEPENDANT_LIVING_UK_PAGE_TITLE);
                Assert.assertTrue(commonPage.getCurrentURL().contains(DEPENDANT_LIVING_UK_PAGE_URL));
                break;
            case "Dependant Name screen":
                Assert.assertEquals(commonPage.getCurrentPageTitle(), DEPENDANT_NAME_PAGE_TITLE);
                Assert.assertTrue(commonPage.getCurrentURL().contains(DEPENDANT_NAME_PAGE_URL));
                break;
            case "Dependant Date of birth screen":
                Assert.assertEquals(commonPage.getCurrentPageTitle(), DEPENDANT_DOB_PAGE_TITLE);
                Assert.assertTrue(commonPage.getCurrentURL().contains(DEPENDANT_DOB_PAGE_URL));
                break;
            case "Dependant IHS Number screen":
                Assert.assertEquals(commonPage.getCurrentPageTitle(), DEPENDANT_IHS_NUMBER_PAGE_TITLE);
                Assert.assertTrue(commonPage.getCurrentURL().contains(DEPENDANT_IHS_NUMBER_PAGE_URL));
                break;
            case "Dependant Visa Share Code screen":
                Assert.assertEquals(commonPage.getCurrentPageTitle(), DEPENDANT_VISA_SHARE_CODE_PAGE_TITLE);
                Assert.assertTrue(commonPage.getCurrentURL().contains(DEPENDANT_VISA_SHARE_CODE_PAGE_URL));
                break;
            case "Same Address as Applicant screen":
                Assert.assertEquals(commonPage.getCurrentPageTitle(), SAME_ADDRESS_AS_APPLICANT_PAGE_TITLE);
                Assert.assertTrue(commonPage.getCurrentURL().contains(SAME_ADDRESS_AS_APPLICANT_PAGE_URL));
                break;
            case "Enter Dependant Address screen":
                Assert.assertEquals(commonPage.getCurrentPageTitle(), ENTER_DEPENDANT_ADDRESS_PAGE_TITLE);
                Assert.assertTrue(commonPage.getCurrentURL().contains(ENTER_DEPENDANT_ADDRESS_PAGE_URL));
                break;
            case "Upload Dependant EHIC Card screen":
                Assert.assertEquals(commonPage.getCurrentPageTitle(), UPLOAD_DEPENDANT_EHIC_PAGE_TITLE);
                Assert.assertTrue(commonPage.getCurrentURL().contains(UPLOAD_DEPENDANT_EHIC_PAGE_URL));
                break;
            case "View Uploaded Dependant EHIC screen":
                Assert.assertEquals(commonPage.getCurrentPageTitle(), VIEW_DEPENDANT_EHIC_PAGE_TITLE);
                Assert.assertTrue(commonPage.getCurrentURL().contains(VIEW_DEPENDANT_EHIC_PAGE_URL));
                break;
            case "Check Dependant Details screen":
                Assert.assertEquals(commonPage.getCurrentPageTitle(), CHECK_DEPENDANTS_PAGE_TITLE);
                Assert.assertTrue(commonPage.getCurrentURL().contains(CHECK_DEPENDANTS_PAGE_URL));
                break;
            case "Check Your Answers screen":
                Assert.assertEquals(commonPage.getCurrentPageTitle(), CHECK_ANSWERS_PAGE_TITLE);
                Assert.assertTrue(commonPage.getCurrentURL().contains(CHECK_ANSWERS_PAGE_URL));
                break;
            case "Declaration screen":
                Assert.assertEquals(commonPage.getCurrentPageTitle(), DECLARATION_PAGE_TITLE);
                Assert.assertTrue(commonPage.getCurrentURL().contains(DECLARATION_PAGE_URL));
                break;
            case "Confirmation screen":
                Assert.assertEquals(commonPage.getCurrentPageTitle(), CONFIRMATION_PAGE_TITLE);
                Assert.assertTrue(commonPage.getCurrentURL().contains(CONFIRMATION_PAGE_URL));
                break;
            case "GOV UK screen":
                Assert.assertEquals(commonPage.getFooterLinksPageTitle(), GOV_UK_PAGE);
                break;
            case "GOV UK View and Prove screen":
                Assert.assertEquals(commonPage.getFooterLinksPageTitle(), REQUEST_SHARE_CODE_PAGE);
                break;
            case "Gov UK Feedback screen":
                Assert.assertEquals(commonPage.getFooterLinksPageTitle(), GOV_UK_FEEDBACK_PAGE);
                break;
            case "Help screen":
                Assert.assertEquals(commonPage.getFooterLinksPageTitle(), HELP_PAGE);
                break;
            case "Contact Us screen":
                Assert.assertEquals(commonPage.getFooterLinksPageTitle(), CONTACT_US_PAGE_TITLE);
                commonPage.navigateToContactUs();
                Assert.assertTrue(commonPage.getFooterLinksPageURL().contains(CONTACT_US_PAGE_URL));
                break;
            case "Cookies Policy screen":
                Assert.assertEquals(commonPage.getFooterLinksPageTitle(), COOKIES_POLICY_PAGE_TITLE);
                commonPage.navigateToCookies();
                Assert.assertTrue(commonPage.getFooterLinksPageURL().contains(COOKIES_POLICY_PAGE_URL));
                break;
            case "Accessibility Statement screen":
                Assert.assertEquals(commonPage.getFooterLinksPageTitle(), ACCESSIBILITY_STATEMENT_PAGE_TITLE);
                commonPage.navigateToAccessibilityStatement();
                Assert.assertTrue(commonPage.getFooterLinksPageURL().contains(ACCESSIBILITY_STATEMENT_PAGE_URL));
                break;
            case "Terms and Conditions screen":
                Assert.assertEquals(commonPage.getFooterLinksPageTitle(), TERMS_CONDITIONS_PAGE_TITLE);
                commonPage.navigateToTermsConditions();
                Assert.assertTrue(commonPage.getFooterLinksPageURL().contains(TERMS_CONDITIONS_PAGE_URL));
                break;
            case "Privacy Notice screen":
                Assert.assertEquals(commonPage.getFooterLinksPageTitle(), PRIVACY_NOTICE_PAGE_TITLE);
                commonPage.navigateToPrivacyNotice();
                Assert.assertTrue(commonPage.getFooterLinksPageURL().contains(PRIVACY_NOTICE_PAGE_URL));
                break;
            case "Blank given name error":
                Assert.assertEquals(namePage.getGivenNameErrorMessage(), "Enter the applicant's given name");
                break;
            case "Blank family name error":
                Assert.assertEquals(namePage.getFamilyNameErrorMessage(), "Enter the applicant's family name");
                break;
            case "Invalid given name error":
                Assert.assertEquals(namePage.getGivenNameErrorMessage(), "Enter the applicant's given name using only letters, hyphens and apostrophes using between 2 and 50 characters");
                break;
            case "Invalid family name error":
                Assert.assertEquals(namePage.getFamilyNameErrorMessage(), "Enter the applicant's family name using only letters, hyphens and apostrophes using between 2 and 50 characters");
                break;
            case "Invalid range given name error":
                Assert.assertEquals(namePage.getGivenNameErrorMessage(), "Enter the applicant's given name using between 2 and 50 characters");
                break;
            case "Invalid range family name error":
                Assert.assertEquals(namePage.getFamilyNameErrorMessage(), "Enter the applicant's family name using between 2 and 50 characters");
                break;
            case "Blank date of birth error":
                Assert.assertEquals(dateOfBirthPage.getDOBErrorMessage(), "Enter the applicant's date of birth");
                break;
            case "Invalid format date of birth error":
                Assert.assertEquals(dateOfBirthPage.getDOBErrorMessage(), "Enter the applicant's date of birth in the correct format, for example, 31 3 1980");
                break;
            case "Minimum 16 years error":
                Assert.assertEquals(dateOfBirthPage.getMinimumYearsErrorMessage(), "Enter a date of birth that is a minimum of 16 years ago");
                break;
            case "Blank ihs number error":
                Assert.assertEquals(ihsNumberPage.getEmptyIHSNumberErrorMessage(), "Enter the applicant's immigration health surcharge number");
                break;
            case "Invalid ihs number error":
                Assert.assertEquals(ihsNumberPage.getIHSNumberErrorMessage(), "Enter a correct immigration health surcharge number");
                break;
            case "Blank share code error":
                Assert.assertEquals(visaShareCodePage.getEmptyVisaShareCodeErrorMessage(), "Enter the applicant's visa share code");
                break;
            case "Invalid share code error":
                Assert.assertEquals(visaShareCodePage.getVisaShareCodeErrorMessage(), "Enter a correct visa share code");
                break;
            case "Blank building error":
                Assert.assertEquals(enterAddressPage.getBuildingErrorMessage(), "Enter the applicant's building name or number and street name");
                break;
            case "Invalid building error":
                Assert.assertEquals(enterAddressPage.getBuildingErrorMessage(), "Enter the applicant's building name or number and street name using only letters, hyphens and apostrophes using between 2 and 35 characters");
                break;
            case "Invalid range building error":
                Assert.assertEquals(enterAddressPage.getBuildingErrorMessage(), "Enter the applicant's building name or number using between 2 and 35 characters");
                break;
            case "Invalid street error":
                Assert.assertEquals(enterAddressPage.getBuildingErrorMessage(), "Enter the applicant's building name or number and street name using only letters, hyphens and apostrophes using between 2 and 35 characters");
                break;
            case "Invalid range street error":
                Assert.assertEquals(enterAddressPage.getBuildingErrorMessage(), "Enter the applicant's building name or number using between 2 and 35 characters");
                break;
            case "Blank town error":
                Assert.assertEquals(enterAddressPage.getTownErrorMessage(), "Enter the applicant's town or city");
                break;
            case "Invalid town error":
                Assert.assertEquals(enterAddressPage.getTownErrorMessage(), "Enter the applicant's town or city using only letters, hyphens and apostrophes using between 2 and 35 characters");
                break;
            case "Invalid range town error":
                Assert.assertEquals(enterAddressPage.getTownErrorMessage(), "Enter the applicant's town or city using between 2 and 35 characters");
                break;
            case "Invalid county error":
                Assert.assertEquals(enterAddressPage.getCountyErrorMessage(), "Enter the applicant's county using only letters, hyphens and apostrophes using between 2 and 35 characters");
                break;
            case "Invalid range county error":
                Assert.assertEquals(enterAddressPage.getCountyErrorMessage(), "Enter the applicant's county using between 2 and 35 characters");
                break;
            case "Blank post code error":
                Assert.assertEquals(enterAddressPage.getPostCodeErrorMessage(), "Enter the applicant's postcode");
                break;
            case "Wrong post code error":
                Assert.assertEquals(enterAddressPage.getWrongPostCodeErrorMessage(), "Enter a valid postcode");
                break;
            case "Blank email address error":
                Assert.assertEquals(emailPage.getEmailErrorMessage(), "Enter an email address that we can use to contact the applicant about the reimbursement");
                break;
            case "Invalid email address error":
                Assert.assertEquals(emailPage.getEmailErrorMessage(), "Enter an email address in the correct format, like name@example.com");
                break;
            case "Invalid range email address error":
                Assert.assertEquals(emailPage.getEmailErrorMessage(), "Enter an email address that is no more than 50 characters");
                break;
            case "Invalid phone number error":
                Assert.assertEquals(mobileNumberPage.getMobileNumberErrorMessage(), "Enter the applicant's telephone number using numbers 0 to 9");
                break;
            case "Invalid range phone number error":
                Assert.assertEquals(mobileNumberPage.getMobileNumberErrorMessage(), "Enter a telephone number using between 11 and 15 characters");
                break;
            case "No file selected error":
                Assert.assertEquals(uploadEHICFilePage.getUploadErrorMessage(), "Select a file to upload as evidence");
                break;
            case "Max file size error":
                Assert.assertEquals(uploadEHICFilePage.getUploadErrorMessage(), "Select a file that has a size no larger than 2MB");
                break;
            case "Invalid file format error":
                Assert.assertEquals(uploadEHICFilePage.getUploadErrorMessage(), "Select a file that is either a jpg, jpeg, bmp, png or pdf");
                break;
            case "Invalid file name error":
                Assert.assertEquals(uploadEHICFilePage.getUploadErrorMessage(), "Select a file where the filename uses only letters, numbers, spaces, hyphens and underscores");
                break;
            case "Dependant question error":
                Assert.assertEquals(addDependantPage.getErrorMessageSelectNoneOption(), "Select 'Yes' if the applicant wants to add any dependants");
                break;
            case "Dependant Paid Work Error Message":
                Assert.assertEquals(dependantPaidWordPage.getErrorMessage(), "Select 'Yes' if the dependant is carrying out any paid work in the UK");
                break;
            case "Dependant Living in UK Error Message":
                Assert.assertEquals(dependantLivingInUKPage.getErrorMessage(), "Select 'Yes' if the dependant is currently living in the UK");
                break;
            case "Blank dependant given name error":
                Assert.assertEquals(dependantNamePage.getGivenNameErrorMessage(), "Enter the dependant's given name");
                break;
            case "Blank dependant family name error":
                Assert.assertEquals(dependantNamePage.getFamilyNameErrorMessage(), "Enter the dependant's family name");
                break;
            case "Invalid dependant given name error":
                Assert.assertEquals(dependantNamePage.getGivenNameErrorMessage(), "Enter the dependant's given name using only letters, hyphens and apostrophes using between 2 and 50 characters");
                break;
            case "Invalid dependant family name error":
                Assert.assertEquals(dependantNamePage.getFamilyNameErrorMessage(), "Enter the dependant's family name using only letters, hyphens and apostrophes using between 2 and 50 characters");
                break;
            case "Invalid range dependant given name error":
                Assert.assertEquals(dependantNamePage.getGivenNameErrorMessage(), "Enter the dependant's given name using between 2 and 50 characters");
                break;
            case "Invalid range dependant family name error":
                Assert.assertEquals(dependantNamePage.getFamilyNameErrorMessage(), "Enter the dependant's family name using between 2 and 50 characters");
                break;
            case "Blank dependant date of birth error":
                Assert.assertEquals(dependantDateOfBirthPage.getDOBErrorMessage(), "Enter the dependant's date of birth");
                break;
            case "Invalid format dependant date of birth error":
                Assert.assertEquals(dependantDateOfBirthPage.getDOBErrorMessage(), "Enter the dependant's date of birth in the correct format, for example, 31 3 1980");
                break;
            case "Future date dependant error":
                Assert.assertEquals(dependantDateOfBirthPage.getMinimumYearsErrorMessage(), "Enter a date of birth that is either today's date or before");
                break;
            case "Blank dependant ihs number error":
                Assert.assertEquals(dependantIHSNumberPage.getEmptyIHSNumberErrorMessage(), "Enter the dependant's immigration health surcharge number");
                break;
            case "Invalid dependant ihs number error":
                Assert.assertEquals(dependantIHSNumberPage.getIHSNumberErrorMessage(), "Enter a correct immigration health surcharge number");
                break;
            case "Blank dependant share code error":
                Assert.assertEquals(dependantVisaShareCodePage.getEmptyVisaShareCodeErrorMessage(), "Enter the dependant's visa share code");
                break;
            case "Invalid dependant share code error":
                Assert.assertEquals(dependantVisaShareCodePage.getVisaShareCodeErrorMessage(), "Enter a correct visa share code");
                break;
            case "Address question error":
                Assert.assertEquals(dependantSameAddressPage.getErrorMessage(), "Select 'Yes' if the dependant lives at the same address as the applicant");
                break;
            case "Blank dependant building error":
                Assert.assertEquals(dependantEnterAddressPage.getBuildingErrorMessage(), "Enter the dependant's building name or number and street name");
                break;
            case "Invalid dependant building error":
                Assert.assertEquals(dependantEnterAddressPage.getBuildingErrorMessage(), "Enter the dependant's building name or number and street name using only letters, hyphens and apostrophes using between 2 and 35 characters");
                break;
            case "Invalid range dependant building error":
                Assert.assertEquals(dependantEnterAddressPage.getBuildingErrorMessage(), "Enter the dependant's building name or number using between 2 and 35 characters");
                break;
            case "Invalid dependant street error":
                Assert.assertEquals(dependantEnterAddressPage.getBuildingErrorMessage(), "Enter the dependant's building name or number and street name using only letters, hyphens and apostrophes using between 2 and 35 characters");
                break;
            case "Invalid range dependant street error":
                Assert.assertEquals(dependantEnterAddressPage.getBuildingErrorMessage(), "Enter the dependant's building name or number using between 2 and 35 characters");
                break;
            case "Blank dependant town error":
                Assert.assertEquals(dependantEnterAddressPage.getTownErrorMessage(), "Enter the dependant's town or city");
                break;
            case "Invalid dependant town error":
                Assert.assertEquals(dependantEnterAddressPage.getTownErrorMessage(), "Enter the dependant's town or city using only letters, hyphens and apostrophes using between 2 and 35 characters");
                break;
            case "Invalid range dependant town error":
                Assert.assertEquals(dependantEnterAddressPage.getTownErrorMessage(), "Enter the dependant's town or city using between 2 and 35 characters");
                break;
            case "Invalid dependant county error":
                Assert.assertEquals(dependantEnterAddressPage.getCountyErrorMessage(), "Enter the dependant's county using only letters, hyphens and apostrophes using between 2 and 35 characters");
                break;
            case "Invalid range dependant county error":
                Assert.assertEquals(dependantEnterAddressPage.getCountyErrorMessage(), "Enter the dependant's county using between 2 and 35 characters");
                break;
            case "Blank dependant post code error":
                Assert.assertEquals(dependantEnterAddressPage.getPostCodeErrorMessage(), "Enter the dependant's postcode");
                break;
            case "Wrong dependant post code error":
                Assert.assertEquals(dependantEnterAddressPage.getWrongPostCodeErrorMessage(), "Enter a valid postcode");
                break;
            case "More Dependants question error":
                Assert.assertEquals(checkDependantsPage.getErrorMessageSelectNoneOption(), "Select 'Yes' if the applicant wants to add any dependants");
                break;
            case "Dependant deleted success message":
                Assert.assertEquals(commonPage.getCurrentPageTitle(), ADD_DEPENDANT_PAGE_TITLE);
                Assert.assertTrue(checkDependantsPage.getDeleteSuccessMessage().contains("Dependant 1 Given Name Family Name has been deleted"));
                break;
        }
    }
}
