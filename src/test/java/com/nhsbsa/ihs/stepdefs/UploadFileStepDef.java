package com.nhsbsa.ihs.stepdefs;

import com.nhsbsa.ihs.driver.Config;
import com.nhsbsa.ihs.pageobjects.*;
import io.cucumber.java.en.And;
import io.cucumber.java.en.When;
import org.junit.Assert;
import org.openqa.selenium.WebDriver;

import static com.nhsbsa.ihs.stepdefs.Constants.FILE_UPLOAD_FILEPATH;

public class UploadFileStepDef {
    private WebDriver driver;
    private UploadEHICFilePage uploadEHICFilePage;
    private UploadCASFilePage uploadCASFilePage;
    private UploadDependantEHICFilePage uploadDependantEHICFilePage;

    public UploadFileStepDef() {
        driver = Config.getDriver();
        uploadEHICFilePage = new UploadEHICFilePage(driver);
        uploadCASFilePage = new UploadCASFilePage(driver);
        uploadDependantEHICFilePage = new UploadDependantEHICFilePage(driver);
    }

    @When("^I upload the EHIC (.*) of format (.*)$")
    public void iUploadTheEHICFileNameOfFormatFileFormat(String fileName, String fileFormat) {
        uploadEHICFilePage.chooseEHICAndContinue(FILE_UPLOAD_FILEPATH + fileName + fileFormat);
    }

    @When("^I upload the EHIC with name (.*) (.*)$")
    public void iUploadTheEHICWithNameFileNameFileFormat(String fileName, String fileFormat) {
        uploadEHICFilePage.chooseEHICAndContinue(FILE_UPLOAD_FILEPATH + fileName + fileFormat);
    }

    @When("^I upload the EHIC (.*) (.*) of size (.*)$")
    public void iUploadTheEHICFile_NameFile_FormatOfSizeFileSize(String fileName, String fileFormat, String fileSize) {
        uploadEHICFilePage.chooseEHICAndContinue(FILE_UPLOAD_FILEPATH + fileName + fileFormat);
    }

    @When("^I (.*) upload the EHIC file$")
    public void iUploadFileUploadTheEHICFile(String uploadFile) {
        switch (uploadFile) {
            case "Do":
                uploadEHICFilePage.chooseEHICAndContinue(FILE_UPLOAD_FILEPATH + "Payslip-June" + ".png");
                break;
            case "Do Not":
                uploadEHICFilePage.continueButton();
                break;
        }
    }

    @And("^I view the uploaded EHIC (.*)$")
    public void iViewTheUploadedEHICFileName(String fileName) {
        Assert.assertTrue(uploadEHICFilePage.getUploadedEHIC().contains(fileName));
        uploadEHICFilePage.continueOnViewEHIC();
    }

    @When("^I upload the CAS (.*) of format (.*)$")
    public void iUploadTheCASFileNameOfFormatFileFormat(String fileName, String fileFormat) {
        uploadCASFilePage.chooseCASAndContinue(FILE_UPLOAD_FILEPATH + fileName + fileFormat);
    }

    @When("^I upload the CAS with name (.*) (.*)$")
    public void iUploadTheCASWithNameFileNameFileFormat(String fileName, String fileFormat) {
        uploadCASFilePage.chooseCASAndContinue(FILE_UPLOAD_FILEPATH + fileName + fileFormat);
    }

    @When("^I upload the CAS (.*) (.*) of size (.*)$")
    public void iUploadTheCASFile_NameFile_FormatOfSizeFileSize(String fileName, String fileFormat, String fileSize) {
        uploadCASFilePage.chooseCASAndContinue(FILE_UPLOAD_FILEPATH + fileName + fileFormat);
    }

    @When("^I (.*) upload the CAS file$")
    public void iUploadFileUploadTheCASFile(String uploadFile) {
        switch (uploadFile) {
            case "Do":
                uploadCASFilePage.chooseCASAndContinue(FILE_UPLOAD_FILEPATH + "Payslip-June" + ".png");
                break;
            case "Do Not":
                uploadCASFilePage.continueButton();
                break;
        }
    }

    @And("^I view the uploaded CAS (.*)$")
    public void iViewTheUploadedCASFileName(String fileName) {
        Assert.assertTrue(uploadCASFilePage.getUploadedCAS().contains(fileName));
        Assert.assertTrue(uploadCASFilePage.getUploadedEHIC().contains("Payslip-June"));
        uploadCASFilePage.continueOnViewCAS();
    }

    @When("^I upload the Dependant EHIC (.*) of format (.*)$")
    public void iUploadTheDependantEHICFileNameOfFormatFileFormat(String fileName, String fileFormat) {
        uploadDependantEHICFilePage.chooseDependantEHICAndContinue(FILE_UPLOAD_FILEPATH + fileName + fileFormat);
    }

    @When("^I upload the Dependant EHIC with name (.*) (.*)$")
    public void iUploadTheDependantEHICWithNameFileNameFileFormat(String fileName, String fileFormat) {
        uploadDependantEHICFilePage.chooseDependantEHICAndContinue(FILE_UPLOAD_FILEPATH + fileName + fileFormat);
    }

    @When("^I upload the Dependant EHIC (.*) (.*) of size (.*)$")
    public void iUploadTheDependantEHICFile_NameFile_FormatOfSizeFileSize(String fileName, String fileFormat, String fileSize) {
        uploadDependantEHICFilePage.chooseDependantEHICAndContinue(FILE_UPLOAD_FILEPATH + fileName + fileFormat);
    }

    @When("^I (.*) upload the Dependant EHIC file$")
    public void iUploadFileUploadTheDependantEHICFile(String uploadFile) {
        switch (uploadFile) {
            case "Do":
                uploadDependantEHICFilePage.chooseDependantEHICAndContinue(FILE_UPLOAD_FILEPATH + "Payslip-June" + ".png");
                break;
            case "Do Not":
                uploadDependantEHICFilePage.continueButton();
                break;
        }
    }

    @And("^I view the uploaded Dependant EHIC (.*)$")
    public void iViewTheUploadedDependantEHICFileName(String fileName) {
        Assert.assertTrue(uploadDependantEHICFilePage.getUploadedEHIC().contains(fileName));
        uploadDependantEHICFilePage.continueOnViewEHIC();
    }
}

