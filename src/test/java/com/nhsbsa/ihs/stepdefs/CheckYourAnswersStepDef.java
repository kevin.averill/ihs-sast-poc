package com.nhsbsa.ihs.stepdefs;

import com.nhsbsa.ihs.driver.Config;
import com.nhsbsa.ihs.pageobjects.*;
import io.cucumber.java.en.And;
import io.cucumber.java.en.When;
import org.junit.Assert;
import org.openqa.selenium.WebDriver;

import static com.nhsbsa.ihs.stepdefs.Constants.*;
import static com.nhsbsa.ihs.stepdefs.Constants.DEPENDANT_POST_CODE;


public class CheckYourAnswersStepDef {
    private WebDriver driver;
    private CheckYourAnswersPage checkYourAnswersPage;
    private CommonPage commonPage;
    private NamePage namePage;
    private DateOfBirthPage dateOfBirthPage;
    private IHSNumberPage ihsNumberPage;
    private VisaShareCodePage visaShareCodePage;
    private EnterAddressPage enterAddressPage;
    private EmailPage emailPage;
    private MobileNumberPage mobileNumberPage;
    private UploadEHICFilePage uploadEHICFilePage;
    private UploadCASFilePage uploadCASFilePage;
    private AddDependantPage addDependantPage;
    private DependantPaidWordPage dependantPaidWordPage;
    private DependantLivingInUKPage dependantLivingInUKPage;
    private DependantNamePage dependantNamePage;
    private DependantDateOfBirthPage dependantDateOfBirthPage;
    private DependantIHSNumberPage dependantIHSNumberPage;
    private DependantVisaShareCodePage dependantVisaShareCodePage;
    private DependantSameAddressPage dependantSameAddressPage;
    private DependantEnterAddressPage dependantEnterAddressPage;
    private UploadDependantEHICFilePage uploadDependantEHICFilePage;
    private CheckDependantsPage checkDependantsPage;


    public CheckYourAnswersStepDef() {
        driver = Config.getDriver();
        checkYourAnswersPage = new CheckYourAnswersPage(driver);
        commonPage = new CommonPage(driver);
        namePage = new NamePage(driver);
        dateOfBirthPage = new DateOfBirthPage(driver);
        ihsNumberPage = new IHSNumberPage(driver);
        visaShareCodePage = new VisaShareCodePage(driver);
        enterAddressPage = new EnterAddressPage(driver);
        emailPage = new EmailPage(driver);
        mobileNumberPage = new MobileNumberPage(driver);
        uploadEHICFilePage = new UploadEHICFilePage(driver);
        uploadCASFilePage = new UploadCASFilePage(driver);
        addDependantPage = new AddDependantPage(driver);
        dependantPaidWordPage = new DependantPaidWordPage(driver);
        dependantLivingInUKPage = new DependantLivingInUKPage(driver);
        dependantNamePage = new DependantNamePage(driver);
        dependantDateOfBirthPage = new DependantDateOfBirthPage(driver);
        dependantIHSNumberPage = new DependantIHSNumberPage(driver);
        dependantVisaShareCodePage = new DependantVisaShareCodePage(driver);
        dependantSameAddressPage = new DependantSameAddressPage(driver);
        dependantEnterAddressPage = new DependantEnterAddressPage(driver);
        uploadDependantEHICFilePage = new UploadDependantEHICFilePage(driver);
        checkDependantsPage = new CheckDependantsPage(driver);
    }

    @And("^My applicant details are captured until Check your answers screen$")
    public void myApplicantDetailsAreCapturedUntilCheckYourAnswersScreen() {
        namePage.enterNameAndSubmit(GIVEN_NAME, FAMILY_NAME);
        dateOfBirthPage.enterDateOfBirthAndSubmit(DOB_DAY, DOB_MONTH, DOB_YEAR);
        ihsNumberPage.enterIHSNumberAndSubmit(IHS_NUMBER);
        visaShareCodePage.enterVisaShareCodeAndSubmit(VISA_SHARE_CODE);
        enterAddressPage.enterAddressAndSubmit(BUILDING_NUMBER, STREET_NAME, TOWN_NAME, COUNTY_NAME, POST_CODE);
        emailPage.enterEmailAndSubmit(EMAIL_ADDRESS);
        mobileNumberPage.enterMobileNumberAndSubmit(PHONE_NUMBER);
        uploadEHICFilePage.chooseEHICAndContinue(FILE_UPLOAD_FILEPATH + "Payslip-June" + ".png");
        uploadEHICFilePage.continueOnViewEHIC();
        uploadCASFilePage.chooseCASAndContinue(FILE_UPLOAD_FILEPATH + "Sample_1" + ".jpg");
        uploadCASFilePage.continueOnViewCAS();
        addDependantPage.selectNoRadioButtonAndClickContinue();
    }

    @And("^My details are captured until Check your answers screen with Dependant$")
    public void myDetailsAreCapturedUntilCheckYourAnswersScreenWithDependant() {
        namePage.enterNameAndSubmit(GIVEN_NAME,FAMILY_NAME);
        dateOfBirthPage.enterDateOfBirthAndSubmit(DOB_DAY,DOB_MONTH,DOB_YEAR);
        ihsNumberPage.enterIHSNumberAndSubmit(IHS_NUMBER);
        visaShareCodePage.enterVisaShareCodeAndSubmit(VISA_SHARE_CODE);
        enterAddressPage.enterAddressAndSubmit(BUILDING_NUMBER, STREET_NAME, TOWN_NAME, COUNTY_NAME, POST_CODE);
        emailPage.enterEmailAndSubmit(EMAIL_ADDRESS);
        mobileNumberPage.enterMobileNumberAndSubmit(PHONE_NUMBER);
        uploadEHICFilePage.chooseEHICAndContinue(FILE_UPLOAD_FILEPATH + "Payslip-June" + ".png");
        uploadEHICFilePage.continueOnViewEHIC();
        uploadCASFilePage.chooseCASAndContinue(FILE_UPLOAD_FILEPATH + "1-payslip_May Name" + ".jpg");
        uploadCASFilePage.continueOnViewCAS();
        addDependantPage.selectYesRadioButtonAndClickContinue();
        dependantPaidWordPage.selectNoRadioButtonAndClickContinue();
        dependantLivingInUKPage.selectYesRadioButtonAndClickContinue();
        dependantNamePage.enterNameAndSubmit(DEPENDANT_GIVEN_NAME,DEPENDANT_FAMILY_NAME);
        dependantDateOfBirthPage.enterDateOfBirthAndSubmit(DEPENDANT_DOB_DAY,DEPENDANT_DOB_MONTH,DEPENDANT_DOB_YEAR);
        dependantIHSNumberPage.enterIHSNumberAndSubmit(DEPENDANT_IHS_NUMBER);
        dependantVisaShareCodePage.enterVisaShareCodeAndSubmit(DEPENDANT_VISA_SHARE_CODE);
        dependantSameAddressPage.selectYesRadioButtonAndClickContinue();
        uploadDependantEHICFilePage.chooseDependantEHICAndContinue(FILE_UPLOAD_FILEPATH + "payslip4" + ".png");
        uploadDependantEHICFilePage.continueOnViewEHIC();
        checkDependantsPage.selectNoRadioButtonAndClickContinue();
    }

    @And("^My details are captured until Check your answers screen with different address for Dependant$")
    public void myDetailsAreCapturedUntilCheckYourAnswersScreenWithDifferentAddressForDependant() {
        namePage.enterNameAndSubmit(GIVEN_NAME,FAMILY_NAME);
        dateOfBirthPage.enterDateOfBirthAndSubmit(DOB_DAY,DOB_MONTH,DOB_YEAR);
        ihsNumberPage.enterIHSNumberAndSubmit(IHS_NUMBER);
        visaShareCodePage.enterVisaShareCodeAndSubmit(VISA_SHARE_CODE);
        enterAddressPage.enterAddressAndSubmit(BUILDING_NUMBER, STREET_NAME, TOWN_NAME, COUNTY_NAME, POST_CODE);
        emailPage.enterEmailAndSubmit(EMAIL_ADDRESS);
        mobileNumberPage.enterMobileNumberAndSubmit(PHONE_NUMBER);
        uploadEHICFilePage.chooseEHICAndContinue(FILE_UPLOAD_FILEPATH + "Payslip-June" + ".png");
        uploadEHICFilePage.continueOnViewEHIC();
        uploadCASFilePage.chooseCASAndContinue(FILE_UPLOAD_FILEPATH + "1-payslip_May Name" + ".jpg");
        uploadCASFilePage.continueOnViewCAS();
        addDependantPage.selectYesRadioButtonAndClickContinue();
        dependantPaidWordPage.selectNoRadioButtonAndClickContinue();
        dependantLivingInUKPage.selectYesRadioButtonAndClickContinue();
        dependantNamePage.enterNameAndSubmit(DEPENDANT_GIVEN_NAME,DEPENDANT_FAMILY_NAME);
        dependantDateOfBirthPage.enterDateOfBirthAndSubmit(DEPENDANT_DOB_DAY,DEPENDANT_DOB_MONTH,DEPENDANT_DOB_YEAR);
        dependantIHSNumberPage.enterIHSNumberAndSubmit(DEPENDANT_IHS_NUMBER);
        dependantVisaShareCodePage.enterVisaShareCodeAndSubmit(DEPENDANT_VISA_SHARE_CODE);
        dependantSameAddressPage.selectNoRadioButtonAndClickContinue();
        dependantEnterAddressPage.enterAddressAndSubmit(DEPENDANT_BUILDING_NUMBER,DEPENDANT_STREET_NAME,DEPENDANT_TOWN_NAME,DEPENDANT_COUNTY_NAME,DEPENDANT_POST_CODE);
        uploadDependantEHICFilePage.chooseDependantEHICAndContinue(FILE_UPLOAD_FILEPATH + "payslip4" + ".png");
        uploadDependantEHICFilePage.continueOnViewEHIC();
        checkDependantsPage.selectNoRadioButtonAndClickContinue();
    }

    @When("^I select (.*) on Check Your Answers page$")
    public void iSelectChangeLinkOnCheckYourAnswersPage(String changeLink) {
        switch (changeLink) {
            case "Change Name link":
                checkYourAnswersPage.claimNameChangeLink();
                break;
            case "Change Date of birth link":
                checkYourAnswersPage.claimDOBChangeLink();
                break;
            case "Change IHS number link":
                checkYourAnswersPage.claimIhsNumberChangeLink();
                break;
            case "Change Visa Share Code link":
                checkYourAnswersPage.claimVisaShareCodeChangeLink();
                break;
            case "Change Address link":
                checkYourAnswersPage.claimAddressChangeLink();
                break;
            case "Change Email address link":
                checkYourAnswersPage.claimEmailChangeLink();
                break;
            case "Change Phone number link":
                checkYourAnswersPage.claimPhoneNumberChangeLink();
                break;
            case "Change Name link for Dependant 1":
                checkYourAnswersPage.depNameChangeLink();
                break;
            case "Change Date of birth link for Dependant 1":
                checkYourAnswersPage.depDOBChangeLink();
                break;
            case "Change IHS number link for Dependant 1":
                checkYourAnswersPage.depIHSNumberChangeLink();
                break;
            case "Change Visa Share Code link for Dependant 1":
                checkYourAnswersPage.depVisaShareCodeChangeLink();
                break;
            case "Change Address link for Dependant 1":
                checkYourAnswersPage.depAddressChangeLink();
                break;

        }
    }

    @When("^I continue without changing my answer on (.*)$")
    public void iContinueWithoutChangingMyAnswerOnScreen(String output) {
        switch (output) {
            case "Name screen":
                namePage.continueButton();
                break;
            case "Date of birth screen":
                dateOfBirthPage.continueButton();
                break;
            case "IHS Number screen":
                ihsNumberPage.continueButton();
                break;
            case "Visa Share Code screen":
                visaShareCodePage.continueButton();
                break;
            case "Enter Address screen":
                enterAddressPage.continueButton();
                break;
            case "Email Address screen":
                emailPage.continueButton();
                break;
            case "Phone number screen":
                mobileNumberPage.continueButton();
                break;
            case "Dependant Living in UK screen":
                dependantLivingInUKPage.continueButton();
                break;
            case "Dependant Name screen":
                dependantNamePage.continueButton();
                break;
            case "Dependant Date of birth screen":
                dependantDateOfBirthPage.continueButton();
                break;
            case "Dependant IHS Number screen":
                dependantIHSNumberPage.continueButton();
                break;
            case "Dependant Visa Share Code screen":
                dependantVisaShareCodePage.continueButton();
                break;
            case "Same Address as Applicant screen":
                dependantSameAddressPage.continueButton();
                break;
            case "Enter Dependant Address screen":
                dependantEnterAddressPage.continueButton();
                break;
        }
    }

    @And("^My answer remains same as on (.*)$")
    public void myAnswerRemainsSameAsOnScreen(String screen) {
        switch (screen) {
            case "Name screen":
                Assert.assertEquals(checkYourAnswersPage.getApplicantName(),GIVEN_NAME +" "+ FAMILY_NAME);
                break;
            case "Date of birth screen":
                Assert.assertEquals(checkYourAnswersPage.getApplicantDOB(),DOB_DAY +" "+ DOB_FULL_MONTH +" "+ DOB_YEAR);
                break;
            case "IHS Number screen":
                Assert.assertEquals(checkYourAnswersPage.getApplicantIHSNumber(), IHS_NUMBER);
                break;
            case "Visa Share Code screen":
                Assert.assertEquals(checkYourAnswersPage.getApplicantVisaShareCode(), VISA_SHARE_CODE);
                break;
            case "Address screen":
                Assert.assertEquals(checkYourAnswersPage.getApplicantAddress(), BUILDING_NUMBER + "\n" +
                        STREET_NAME + "\n" +
                        TOWN_NAME + "\n" +
                        COUNTY_NAME + "\n +" +
                        POST_CODE);
                break;
            case "Email address screen":
                Assert.assertEquals(checkYourAnswersPage.getApplicantEmail(), EMAIL_ADDRESS);
                break;
            case "Phone number screen":
                Assert.assertEquals(checkYourAnswersPage.getApplicantPhoneNumber(), PHONE_NUMBER);
                break;
            case "Dependant Name screen":
                Assert.assertEquals(checkYourAnswersPage.getDepName(), DEPENDANT_GIVEN_NAME + " " + DEPENDANT_FAMILY_NAME);
                break;
            case "Dependant Date of birth screen":
                Assert.assertEquals(checkYourAnswersPage.getDepDOB(), DEPENDANT_DOB_DAY + " " + DEPENDANT_DOB_FULL_MONTH + " " + DEPENDANT_DOB_YEAR);
                break;
            case "Dependant IHS Number screen":
                Assert.assertEquals(checkYourAnswersPage.getDepIHSNumber(), DEPENDANT_IHS_NUMBER);
                break;
            case "Dependant Visa Share Code screen":
                Assert.assertEquals(checkYourAnswersPage.getDepVisaShareCode(), DEPENDANT_VISA_SHARE_CODE);
                break;
            case "Same Address as Applicant screen":
                Assert.assertTrue(dependantSameAddressPage.isYesRadioButtonSelected());
                Assert.assertEquals(checkYourAnswersPage.getDepAddress(), BUILDING_NUMBER + "\n" +
                        STREET_NAME + "\n" +
                        TOWN_NAME + "\n" +
                        COUNTY_NAME + "\n +" +
                        POST_CODE);
                break;
            case "Enter Dependant Address screen":
                Assert.assertEquals(checkYourAnswersPage.getDepAddress(), BUILDING_NUMBER + "\n" +
                        STREET_NAME + "\n" +
                        TOWN_NAME + "\n" +
                        COUNTY_NAME + "\n" +
                        POST_CODE);
                break;
        }
    }

    @And("^My answer remains same on (.*) having different address$")
    public void myAnswerRemainsSameOnScreenHavingDifferentAddress(String screen) {
        switch (screen) {
            case "Name screen":
                Assert.assertEquals(checkYourAnswersPage.getApplicantName(),GIVEN_NAME +" "+ FAMILY_NAME);
                break;
            case "Date of birth screen":
                Assert.assertEquals(checkYourAnswersPage.getApplicantDOB(),DOB_DAY +" "+ DOB_FULL_MONTH +" "+ DOB_YEAR);
                break;
            case "IHS Number screen":
                Assert.assertEquals(checkYourAnswersPage.getApplicantIHSNumber(), IHS_NUMBER);
                break;
            case "Visa Share Code screen":
                Assert.assertEquals(checkYourAnswersPage.getApplicantVisaShareCode(), VISA_SHARE_CODE);
                break;
            case "Address screen":
                Assert.assertEquals(checkYourAnswersPage.getApplicantAddress(), BUILDING_NUMBER + "\n" +
                        STREET_NAME + "\n" +
                        TOWN_NAME + "\n" +
                        COUNTY_NAME + "\n +" +
                        POST_CODE);
                break;
            case "Email address screen":
                Assert.assertEquals(checkYourAnswersPage.getApplicantEmail(), EMAIL_ADDRESS);
                break;
            case "Phone number screen":
                Assert.assertEquals(checkYourAnswersPage.getApplicantPhoneNumber(), PHONE_NUMBER);
                break;
            case "Dependant Name screen":
                Assert.assertEquals(checkYourAnswersPage.getDepName(), DEPENDANT_GIVEN_NAME + " " + DEPENDANT_FAMILY_NAME);
                break;
            case "Dependant Date of birth screen":
                Assert.assertEquals(checkYourAnswersPage.getDepDOB(), DEPENDANT_DOB_DAY + " " + DEPENDANT_DOB_FULL_MONTH + " " + DEPENDANT_DOB_YEAR);
                break;
            case "Dependant IHS Number screen":
                Assert.assertEquals(checkYourAnswersPage.getDepIHSNumber(), DEPENDANT_IHS_NUMBER);
                break;
            case "Dependant Visa Share Code screen":
                Assert.assertEquals(checkYourAnswersPage.getDepVisaShareCode(), DEPENDANT_VISA_SHARE_CODE);
                break;
            case "Enter Dependant Address screen":
                Assert.assertEquals(checkYourAnswersPage.getDepAddress(), DEPENDANT_BUILDING_NUMBER + "\n" +
                        DEPENDANT_STREET_NAME + "\n" +
                        DEPENDANT_TOWN_NAME + "\n" +
                        DEPENDANT_COUNTY_NAME + "\n" +
                        DEPENDANT_POST_CODE);
                break;
        }
    }

    @And("^My claimant's (.*) is pre-populated$")
    public void myClaimantSAnswerIsPrePopulated(String answer) {
        switch (answer) {
            case "Name":
                Assert.assertEquals(namePage.getEnteredGivenName(), GIVEN_NAME);
                Assert.assertEquals(namePage.getEnteredFamilyName(), FAMILY_NAME);
                break;
            case "Date of birth":
                Assert.assertEquals(dateOfBirthPage.getEnteredDay(), DOB_DAY);
                Assert.assertEquals(dateOfBirthPage.getEnteredMonth(), DOB_MONTH);
                Assert.assertEquals(dateOfBirthPage.getEnteredYear(), DOB_YEAR);
                break;
            case "IHS Number":
                Assert.assertEquals(ihsNumberPage.getEnteredIHSNumber(), IHS_NUMBER);
                break;
            case "Visa Share Code":
                Assert.assertEquals(visaShareCodePage.getEnteredVisaShareCode(), VISA_SHARE_CODE);
                break;
            case "Address":
                Assert.assertEquals(enterAddressPage.getEnteredBuilding(), BUILDING_NUMBER);
                Assert.assertEquals(enterAddressPage.getEnteredStreet(), STREET_NAME);
                Assert.assertEquals(enterAddressPage.getEnteredTown(), TOWN_NAME);
                Assert.assertEquals(enterAddressPage.getEnteredCounty(), COUNTY_NAME);
                Assert.assertEquals(enterAddressPage.getEnteredPostCode(), POST_CODE);
                break;
            case "Email":
                Assert.assertEquals(emailPage.getEnteredEmail(), EMAIL_ADDRESS);
                break;
            case "Phone":
                Assert.assertEquals(mobileNumberPage.getEnteredPhoneNumber(), PHONE_NUMBER);
            case "Dep Name":
                Assert.assertEquals(dependantNamePage.getEnteredGivenName(), DEPENDANT_GIVEN_NAME);
                Assert.assertEquals(dependantNamePage.getEnteredFamilyName(), DEPENDANT_FAMILY_NAME);
                break;
            case "Dep Date of Birth":
                Assert.assertEquals(dependantDateOfBirthPage.getEnteredDay(), DEPENDANT_DOB_DAY);
                Assert.assertEquals(dependantDateOfBirthPage.getEnteredMonth(), DEPENDANT_DOB_MONTH);
                Assert.assertEquals(dependantDateOfBirthPage.getEnteredYear(), DEPENDANT_DOB_YEAR);
                break;
            case "Dep IHS Number":
                Assert.assertEquals(dependantIHSNumberPage.getEnteredIHSNumber(), DEPENDANT_IHS_NUMBER);
                break;
            case "Dep Visa Share Code":
                Assert.assertEquals(dependantVisaShareCodePage.getEnteredShareCode(), DEPENDANT_VISA_SHARE_CODE);
                break;
            case "Dep Same Address":
                Assert.assertEquals(dependantSameAddressPage.isYesRadioButtonSelected(),true);
            case "Dep With Same Address":
                Assert.assertEquals(dependantEnterAddressPage.getEnteredBuilding(), BUILDING_NUMBER);
                Assert.assertEquals(dependantEnterAddressPage.getEnteredStreet(), STREET_NAME);
                Assert.assertEquals(dependantEnterAddressPage.getEnteredTown(), TOWN_NAME);
                Assert.assertEquals(dependantEnterAddressPage.getEnteredCounty(), COUNTY_NAME);
                Assert.assertEquals(dependantEnterAddressPage.getEnteredPostCode(), POST_CODE);
                break;
            case "Dep With Different Address":
                Assert.assertEquals(dependantEnterAddressPage.getEnteredBuilding(), DEPENDANT_BUILDING_NUMBER);
                Assert.assertEquals(dependantEnterAddressPage.getEnteredStreet(), DEPENDANT_STREET_NAME);
                Assert.assertEquals(dependantEnterAddressPage.getEnteredTown(), DEPENDANT_TOWN_NAME);
                Assert.assertEquals(dependantEnterAddressPage.getEnteredCounty(), DEPENDANT_COUNTY_NAME);
                Assert.assertEquals(dependantEnterAddressPage.getEnteredPostCode(), DEPENDANT_POST_CODE);
                break;
        }
    }

    @When("^I change my (.*) and continue$")
    public void iChangeMyAnswerAndContinue(String answer) {
        switch (answer){
            case "Name":
                namePage.enterNameAndSubmit(UPDATED_GIVEN_NAME,UPDATED_FAMILY_NAME);
                break;
            case "Date of birth":
                dateOfBirthPage.enterDateOfBirthAndSubmit(UPDATED_DOB_DAY,UPDATED_DOB_MONTH,UPDATED_DOB_YEAR);
                break;
            case "IHS Number":
                ihsNumberPage.enterIHSNumberAndSubmit(UPDATED_IHS_NUMBER);
                break;
            case "Visa Share Code":
                visaShareCodePage.enterVisaShareCodeAndSubmit(UPDATED_VISA_SHARE_CODE);
                break;
            case "Address":
                enterAddressPage.enterAddressAndSubmit(UPDATED_BUILDING_NUMBER,UPDATED_STREET_NAME,UPDATED_TOWN_NAME,UPDATED_COUNTY_NAME,UPDATED_POST_CODE);
                break;
            case "Email":
                emailPage.enterEmailAndSubmit(UPDATED_EMAIL_ADDRESS);
                break;
            case "Phone Number":
                mobileNumberPage.enterMobileNumberAndSubmit(UPDATED_PHONE_NUMBER);
                break;
            case "Dep Name":
                dependantNamePage.enterNameAndSubmit(UPDATED_DEPENDANT_GIVEN_NAME,UPDATED_DEPENDANT_FAMILY_NAME);
                break;
            case "Dep Date of Birth":
                dependantDateOfBirthPage.enterDateOfBirthAndSubmit(UPDATED_DEPENDANT_DOB_DAY,UPDATED_DEPENDANT_DOB_MONTH,UPDATED_DEPENDANT_DOB_YEAR);
                break;
            case "Dep IHS Number":
                dependantIHSNumberPage.enterIHSNumberAndSubmit(UPDATED_DEPENDANT_IHS_NUMBER);
                break;
            case "Dep Visa Share Code":
                dependantVisaShareCodePage.enterVisaShareCodeAndSubmit(UPDATED_DEPENDANT_VISA_SHARE_CODE);
                break;
            case "Dep With Same Address":
                dependantSameAddressPage.selectNoRadioButtonAndClickContinue();
                break;
            case "Dep With Different Address":
                dependantEnterAddressPage.enterAddressAndSubmit(UPDATED_DEPENDANT_BUILDING_NUMBER, UPDATED_DEPENDANT_STREET_NAME, UPDATED_DEPENDANT_TOWN_NAME, UPDATED_DEPENDANT_COUNTY_NAME, UPDATED_DEPENDANT_POST_CODE);
                break;
        }
    }

    @And("^I see the updated (.*) on Check Your Answers screen$")
    public void iSeeTheUpdatedAnswersOnCheckYourAnswersScreen(String answer) {
        switch (answer) {
            case "Name":
                Assert.assertEquals(checkYourAnswersPage.getApplicantName(),UPDATED_GIVEN_NAME + " " + UPDATED_FAMILY_NAME);
                break;
            case "Date of birth":
                Assert.assertEquals(checkYourAnswersPage.getApplicantDOB(), UPDATED_DOB_DAY + " " + UPDATED_FULL_DOB_MONTH + " " + UPDATED_DOB_YEAR);
                break;
            case "IHS Number":
                Assert.assertEquals(checkYourAnswersPage.getApplicantIHSNumber(), UPDATED_IHS_NUMBER);
                break;
            case "Visa Share Code":
                Assert.assertEquals(checkYourAnswersPage.getApplicantVisaShareCode(), UPDATED_VISA_SHARE_CODE);
                break;
            case "Address":
                Assert.assertEquals(checkYourAnswersPage.getApplicantAddress(), UPDATED_BUILDING_NUMBER + "\n" +
                        UPDATED_STREET_NAME + "\n" +
                        UPDATED_TOWN_NAME + "\n" +
                        UPDATED_COUNTY_NAME + "\n" +
                        UPDATED_POST_CODE);
                break;
            case "Email":
                Assert.assertEquals(checkYourAnswersPage.getApplicantEmail(), UPDATED_EMAIL_ADDRESS);
                break;
            case "Phone Number":
                Assert.assertEquals(checkYourAnswersPage.getApplicantPhoneNumber(), UPDATED_PHONE_NUMBER);
                break;
            case "Dep Name":
                Assert.assertEquals(checkYourAnswersPage.getDepName(),UPDATED_DEPENDANT_GIVEN_NAME + " " + UPDATED_DEPENDANT_FAMILY_NAME);
                break;
            case "Dep Date of Birth":
                Assert.assertEquals(checkYourAnswersPage.getDepDOB(), UPDATED_DEPENDANT_DOB_DAY + " " + UPDATED_DEPENDANT_DOB_FULL_MONTH + " " + UPDATED_DEPENDANT_DOB_YEAR);
                break;
            case "Dep IHS Number":
                Assert.assertEquals(checkYourAnswersPage.getDepIHSNumber(), UPDATED_DEPENDANT_IHS_NUMBER);
                break;
            case "Dep Visa Share Code":
                Assert.assertEquals(checkYourAnswersPage.getDepVisaShareCode(), UPDATED_DEPENDANT_VISA_SHARE_CODE);
                break;
            case "Dep With Different Address":
                Assert.assertEquals(checkYourAnswersPage.getDepAddress(), UPDATED_DEPENDANT_BUILDING_NUMBER + "\n" +
                        UPDATED_DEPENDANT_STREET_NAME + "\n" +
                        UPDATED_DEPENDANT_TOWN_NAME + "\n" +
                        UPDATED_DEPENDANT_COUNTY_NAME + "\n" +
                        UPDATED_DEPENDANT_POST_CODE);
                break;
        }

    }

    @And("^I change my (.*) and do not continue$")
    public void iChangeMyAnswerAndDoNotContinue(String answer) throws InterruptedException {
        switch (answer){
            case "Name":
                namePage.enterName(UPDATED_GIVEN_NAME, UPDATED_FAMILY_NAME);
                break;
            case "Date of birth":
                dateOfBirthPage.enterDateOfBirth(UPDATED_DOB_DAY, UPDATED_DOB_MONTH, UPDATED_DOB_YEAR);
                break;
            case "IHS Number":
                ihsNumberPage.enterIHSNumber(UPDATED_IHS_NUMBER);
                break;
            case "Visa Share Code":
                visaShareCodePage.enterVisaShareCode(UPDATED_VISA_SHARE_CODE);
                break;
            case "Address":
                enterAddressPage.enterAddress(UPDATED_BUILDING_NUMBER,UPDATED_STREET_NAME,UPDATED_TOWN_NAME,UPDATED_COUNTY_NAME,UPDATED_POST_CODE);
                break;
            case "Email":
                emailPage.enterEmail(UPDATED_EMAIL_ADDRESS);
                break;
            case "Phone Number":
                mobileNumberPage.enterMobileNumber(UPDATED_PHONE_NUMBER);
                break;
            case "Dep Name":
                dependantNamePage.enterName(UPDATED_DEPENDANT_GIVEN_NAME,UPDATED_DEPENDANT_FAMILY_NAME);
                break;
            case "Dep Date of Birth":
                dependantDateOfBirthPage.enterDateOfBirth(UPDATED_DOB_DAY, UPDATED_DOB_MONTH, UPDATED_DOB_YEAR);
                break;
            case "Dep IHS Number":
                dependantIHSNumberPage.enterIHSNumber(UPDATED_IHS_NUMBER);
                break;
            case "Dep Visa Share Code":
                dependantVisaShareCodePage.enterVisaShareCode(UPDATED_VISA_SHARE_CODE);
                break;
            case "Dep Address":
                dependantEnterAddressPage.enterAddress(UPDATED_DEPENDANT_BUILDING_NUMBER,UPDATED_DEPENDANT_STREET_NAME,UPDATED_DEPENDANT_TOWN_NAME,UPDATED_DEPENDANT_COUNTY_NAME,UPDATED_DEPENDANT_POST_CODE);
                break;

        }
    }
}
