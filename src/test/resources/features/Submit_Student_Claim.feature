@EndToEnd @SubmitClaim @Regression @Smoke

Feature: Validation of the end to end claim submission in the IHS student claim application

  Scenario Outline: Verify that the user is able to submit a new student claim without dependant
    Given I launch the IHS Student claim application
    When My firstname is <GivenName> and surname is <FamilyName>
    And My date of birth is <Day> <Month> <Year>
    And My IHS number is <IHSNumber>
    And My visa share code is <VisaCode>
    And My address is <Building> <Street> <Town> <County> and <Postcode>
    And My Email address is <Email>
    And My Phone number is <PhoneNumber>
    And I upload the EHIC <fileName> of format <fileFormat>
    And I view the uploaded EHIC <fileName>
    And I upload the CAS <fileName> of format <fileFormat>
    And I view the uploaded CAS <fileName>
    And I <dependantOption> have dependant to add
    And I check my answers and submit the claim application
    Then My claim is submitted successfully

    Examples:
      | GivenName | FamilyName | Day | Month | Year | IHSNumber    | VisaCode    | Building | Street      | Town   | County    | Postcode | Email                      | PhoneNumber | dependantOption | fileName     | fileFormat |
      | Henry     | O'neil     | 07  | 04    | 1988 | IHS098765432 | A12 345 67Z | 123      | Test Street | London | Wiltshire | EC1A 1BB | nhsbsa.ihs-testing@nhs.net | 07123456789 | do not          | Payslip-June | .png       |

  Scenario Outline: Verify that the user is able to submit a new student claim with 3 dependants
    Given I launch the IHS Student claim application
    When My firstname is <GivenName> and surname is <FamilyName>
    And My date of birth is <Day> <Month> <Year>
    And My IHS number is <IHSNumber>
    And My visa share code is <VisaCode>
    And My address is <Building> <Street> <Town> <County> and <Postcode>
    And My Email address is <Email>
    And My Phone number is <PhoneNumber>
    And I upload the EHIC <fileName> of format <fileFormat>
    And I view the uploaded EHIC <fileName>
    And I upload the CAS <fileName> of format <fileFormat>
    And I view the uploaded CAS <fileName>
    And I <dependantOption> have dependant to add
    And I add <count> dependants to my claim application
    And I check my answers and submit the claim application
    Then My claim is submitted successfully

    Examples:
      | GivenName | FamilyName | Day | Month | Year | IHSNumber    | VisaCode    | Building | Street      | Town   | County    | Postcode | Email                      | PhoneNumber | dependantOption | count | fileName     | fileFormat |
      | Melissa   | Cooper     | 21  | 11    | 1988 | IHS567456734 | A98 765 43Z | 123      | Test Street | London | Wiltshire | EC1A 1BB | nhsbsa.ihs-testing@nhs.net | 07123456789 | do              | 3     | Payslip-June | .png       |