@UploadCAS @IHSRI-1095 @Regression

Feature: Validation of the Upload CAS Letter page on the IHS claim app to enable student applicants to upload their CAS letter to prove their eligibility.

  Background:
    Given I launch the IHS Student claim application
    And My details are captured until Upload CAS screen

  @Smoke
  Scenario Outline: Validate the upload CAS functionality for all the acceptable 'File' formats
    When I upload the CAS <fileName> of format <fileFormat>
    Then I will see the <output>
    Examples:
      | fileName     | fileFormat | output                        |
      #valid file formats
      | Sample_1     | .jpg       | View Uploaded Evidence screen |
      | sample_2     | .png       | View Uploaded Evidence screen |
      | sample_3     | .pdf       | View Uploaded Evidence screen |
      | Sample_4     | .bmp       | View Uploaded Evidence screen |
      | Sample_5     | .jpeg      | View Uploaded Evidence screen |
      #invalid file formats
      | Sample excel | .xlsx      | Invalid file format error     |
      | Sample text  | .rtf       | Invalid file format error     |
      | Sample word  | .doc       | Invalid file format error     |

  Scenario Outline: Validate the upload CAS functionality for all the acceptable 'File Name' formats
    When I upload the CAS with name <fileName> <fileFormat>
    Then I will see the <output>
    Examples:
      | fileName           | fileFormat | output                        |
      #valid file names
      | Payslip-June       | .png       | View Uploaded Evidence screen |
      | My Payslip_1       | .pdf       | View Uploaded Evidence screen |
      | Payslip July       | .bmp       | View Uploaded Evidence screen |
      | 123456             | .pdf       | View Uploaded Evidence screen |
#      | 1-payslip_May Name | .jpg       | View Uploaded Evidence screen |
      | Payslip   Mar      | .pdf       | View Uploaded Evidence screen |
      #invalid file names
      | 'My Payslip'       | .jpg       | Invalid file name error       |
      | Payslip(Nov)       | .png       | Invalid file name error       |
      | Payslip.Jan        | .pdf       | Invalid file name error       |

  Scenario Outline: Validate the error when CAS file with size larger than 2MB is uploaded
    When I upload the CAS <fileName> <fileFormat> of size <fileSize>
    Then I will see the <output>
    Examples:
      | fileName       | fileFormat | fileSize | output              |
      | More_than_2MB  | .bmp       | > 2MB    | Max file size error |
      | More_than_2MB  | .jpeg      | > 2MB    | Max file size error |
      | More_than_2MB  | .jpg       | > 2MB    | Max file size error |
      | More_than_2MB  | .pdf       | > 2MB    | Max file size error |
      | More_than_2MB  | .png       | > 2MB    | Max file size error |
      | More_than_5MB  | .bmp       | > 5MB    | Max file size error |
      | More_than_5MB  | .pdf       | > 5MB    | Max file size error |
      | More_than_10MB | .bmp       | > 10MB   | Max file size error |
      | More_than_10MB | .jpeg      | > 10MB   | Max file size error |
      | More_than_10MB | .jpg       | > 10MB   | Max file size error |
      | More_than_10MB | .pdf       | > 10MB   | Max file size error |
      | More_than_30MB | .pdf       | > 30MB   | Max file size error |
      | More_than_30MB | .png       | > 30MB   | Max file size error |
      | More_than_50MB | .bmp       | > 50MB   | Max file size error |

  Scenario Outline: Validate the user is able to upload the CAS file
    When I <uploadFile> upload the CAS file
    Then I will see the <output>
    Examples:
      | uploadFile | output                        |
      | Do         | View Uploaded Evidence screen |
      | Do Not     | No file selected error        |

  Scenario Outline: Validate the hyperlinks on Upload CAS page
    When I select the <hyperlink>
    Then I will see the <output>
    Examples:
      | hyperlink         | output                    |
      | Back link         | View Uploaded EHIC screen |
      | Service Name link | Start screen              |
      | GOV.UK link       | GOV UK screen             |