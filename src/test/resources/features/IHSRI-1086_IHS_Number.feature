@IHSNumber @IHSRI-1086 @Regression

Feature: Validation of IHS Number Page on the IHS claim app to enable student applicants to enter their IHS number for the claim application

  Background:
    Given I launch the IHS Student claim application
    And My details are captured until IHS Number screen

  @Retest-1295
  Scenario Outline: Validate the enter IHS number functionality for positive and negative scenarios
    When My IHS number is <IHSNumber>
    Then I will see the <output>
    Examples:
      | IHSNumber     | output                   |
      #positive tests
      | IHS123456789  | Visa Share Code screen   |
      | ihs887744339  | Visa Share Code screen   |
      | IHS7896543214 | Visa Share Code screen   |
      #negative tests
      |               | Blank ihs number error   |
      | IHS78965456   | Invalid ihs number error |
      | 12345678      | Invalid ihs number error |
      | 1234567898    | Invalid ihs number error |
      | IGS123456789  | Invalid ihs number error |
      | asvboiubg     | Invalid ihs number error |

  Scenario Outline: Validate the hyperlinks on IHS Number page
    When I select the <hyperlink>
    Then I will see the <output>
    Examples:
      | hyperlink         | output               |
      | Back link         | Date of birth screen |
      | Service Name link | Start screen         |
      | GOV.UK link       | GOV UK screen        |