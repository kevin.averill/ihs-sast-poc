@DependantIHSNumber @IHSRI-1099 @Regression

Feature: Validation of Dependant IHS Number Page on the IHS claim app to enable student applicants to enter their dependant's IHS number for the claim application

  Background:
    Given I launch the IHS Student claim application
    And My details are captured until Dependant IHS Number screen

  Scenario Outline: Validate the enter IHS number functionality for positive and negative scenarios
    When My dependant's IHS number is <IHSNumber>
    Then I will see the <output>
    Examples:
      | IHSNumber     | output                             |
      #positive tests
      | IHS123456789  | Dependant Visa Share Code screen   |
      | ihs887744339  | Dependant Visa Share Code screen   |
      | IHS7896543214 | Dependant Visa Share Code screen   |
      #negative tests
      |               | Blank dependant ihs number error   |
      | IHS78965456   | Invalid dependant ihs number error |
      | 12345678      | Invalid dependant ihs number error |
      | 1234567898    | Invalid dependant ihs number error |
      | IGS123456789  | Invalid dependant ihs number error |
      | asvboiubg     | Invalid dependant ihs number error |

  Scenario Outline: Validate the hyperlinks on Dependant IHS Number page
    When I select the <hyperlink>
    Then I will see the <output>
    Examples:
      | hyperlink         | output                         |
      | Back link         | Dependant Date of birth screen |
      | Service Name link | Start screen                   |
      | GOV.UK link       | GOV UK screen                  |