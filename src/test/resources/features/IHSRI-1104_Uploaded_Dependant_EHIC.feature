@ViewDependantEHIC @IHSRI-1104 @Regression

Feature: Validation of the Uploaded Dependant EHIC Card page on the IHS claim app to enable student applicants to view their dependant's EHIC card to prove their eligibility.

  Background:
    Given I launch the IHS Student claim application
    And My details are captured until Upload Dependant EHIC screen

  @Smoke
  Scenario Outline: Validate the user is able to view the uploaded EHIC file
    When I upload the Dependant EHIC <fileName> of format <fileFormat>
    And I view the uploaded Dependant EHIC <fileName>
    Then I will see the <output>
    Examples:
      | fileName             | fileFormat | output                         |
      | Sample_4             | .bmp       | Check Dependant Details screen |
      | sample_3             | .pdf       | Check Dependant Details screen |
      | Payslip-June         | .png       | Check Dependant Details screen |
#      | Large_size_jpeg_file | .jpeg      | Check Dependant Details screen |
#      | Large_size_jpg_file  | .jpg       | Check Dependant Details screen |
#      | Large_size_png_file  | .png       | Check Dependant Details screen |

  Scenario Outline: Validate the hyperlinks on View Dependant's EHIC page
    When I <uploadFile> upload the Dependant EHIC file
    And I select the <hyperlink>
    Then I will see the <output>
    Examples:
      | uploadFile | hyperlink         | output        |
      | Do         | Service Name link | Start screen  |
      | Do         | GOV.UK link       | GOV UK screen |