@CheckDependants @IHSRI-1273 @Regression

Feature: Validation of the Check Dependants Page on the IHS claim app to enable student applicants to check the details of their dependants

  Background:
    Given I launch the IHS Student claim application
    And My details are captured until Check Dependants screen

  Scenario Outline: Validate the user is able to add multiple dependants to the claim application
    When I <dependantMoreOption> have another dependant to add
    Then I will see the <output>
    Examples:
      | dependantMoreOption | output                         |
      | do                  | Dependant Paid Work screen     |
      | do not              | Check Your Answers screen      |
      |                     | More Dependants question error |

  @Smoke
  Scenario Outline: Validate the hyperlinks on Check Dependants page
    When I select the <hyperlink>
    Then I will see the <output>
    Examples:
      | hyperlink             | output                            |
      | Delete Dependant link | Dependant deleted success message |
      | Service Name link     | Start screen                      |
      | GOV.UK link           | GOV UK screen                     |