@CheckYourAnswers @IHSRI-1308 @Regression

Feature: Validation of Change Your Answers functionality on Claim web app to enable users to change their answers before submitting the claim application

  Background:
    Given I launch the IHS Student claim application

  Scenario Outline: Validate the change links on Check Your Answers page without Dependant - do not change the answers and verify answer remains same
    And My applicant details are captured until Check your answers screen
    When I select <changeLink> on Check Your Answers page
    Then I will see the <screen>
    And My claimant's <answer> is pre-populated
    When I continue without changing my answer on <screen>
    Then I will see the Check Your Answers screen
    And My answer remains same as on <screen>

    Examples:
      | changeLink                  | screen                 | answer          |
      | Change Name link            | Name screen            | Name            |
      | Change Date of birth link   | Date of birth screen   | Date of birth   |
      | Change IHS number link      | IHS Number screen      | IHS Number      |
      | Change Visa Share Code link | Visa Share Code screen | Visa Share Code |
      | Change Address link         | Enter Address screen   | Address         |
      | Change Email address link   | Email Address screen   | Email           |
      | Change Phone number link    | Phone number screen    | Phone Number    |

  Scenario Outline: Validate the change links on Check Your Answers page with Dependant having same address as applicant - do not change the answers and verify answer remains same
    And My details are captured until Check your answers screen with Dependant
    And I will see the Check Your Answers screen
    When I select <changeLink> on Check Your Answers page
    Then I will see the <screen>
    And My claimant's <answer> is pre-populated
    When I continue without changing my answer on <screen>
    Then I will see the Check Your Answers screen
    And My answer remains same as on <screen>

    Examples:
      | changeLink                                  | screen                           | answer                |
      | Change Name link                            | Name screen                      | Name                  |
      | Change Date of birth link                   | Date of birth screen             | Date of birth         |
      | Change IHS number link                      | IHS Number screen                | IHS Number            |
      | Change Visa Share Code link                 | Visa Share Code screen           | Visa Share Code       |
      | Change Address link                         | Enter Address screen             | Address               |
      | Change Email address link                   | Email Address screen             | Email                 |
      | Change Phone number link                    | Phone number screen              | Phone Number          |
      | Change Name link for Dependant 1            | Dependant Name screen            | Dep Name              |
      | Change Date of birth link for Dependant 1   | Dependant Date of birth screen   | Dep Date of Birth     |
      | Change IHS number link for Dependant 1      | Dependant IHS Number screen      | Dep IHS Number        |
      | Change Visa Share Code link for Dependant 1 | Dependant Visa Share Code screen | Dep Visa Share Code   |
      | Change Address link for Dependant 1         | Enter Dependant Address screen   | Dep With Same Address |

  Scenario Outline: Validate the change links on Check Your Answers page with Dependant having different address - do not change the answers and verify answer remains same
    And My details are captured until Check your answers screen with different address for Dependant
    And I will see the Check Your Answers screen
    When I select <changeLink> on Check Your Answers page
    Then I will see the <screen>
    And My claimant's <answer> is pre-populated
    When I continue without changing my answer on <screen>
    Then I will see the Check Your Answers screen
    And My answer remains same on <screen> having different address

    Examples:
      | changeLink                                  | screen                           | answer                     |
      | Change Address link for Dependant 1         | Enter Dependant Address screen   | Dep With Different Address |

  Scenario Outline: Validate the back link functionality on Check Your Answers page without changing the answers
    And My details are captured until Check your answers screen with Dependant
    When I select <changeLink> on Check Your Answers page
    Then I will see the <screen>
    And My claimant's <answer> is pre-populated
    When I select the Back link
    Then I will see the <previous screen>
    When I continue without changing my answer on <previous screen>
    Then I will see the Check Your Answers screen
    And My answer remains same as on <screen>

    Examples:
      | changeLink                                  | screen                           | previous screen                  | answer                |
      | Change Date of birth link                   | Date of birth screen             | Name screen                      | Date of birth         |
      | Change IHS number link                      | IHS Number screen                | Date of birth screen             | IHS Number            |
      | Change Visa Share Code link                 | Visa Share Code screen           | IHS Number screen                | Visa Share Code       |
      | Change Address link                         | Enter Address screen             | Visa Share Code screen           | Address               |
      | Change Email address link                   | Email Address screen             | Enter Address screen             | Email                 |
      | Change Phone number link                    | Phone number screen              | Email Address screen             | Phone Number          |
      | Change Name link for Dependant 1            | Dependant Name screen            | Dependant Living in UK screen    | Dep Name              |
      | Change Date of birth link for Dependant 1   | Dependant Date of birth screen   | Dependant Name screen            | Dep Date of Birth     |
      | Change IHS number link for Dependant 1      | Dependant IHS Number screen      | Dependant Date of birth screen   | Dep IHS Number        |
      | Change Visa Share Code link for Dependant 1 | Dependant Visa Share Code screen | Dependant IHS Number screen      | Dep Visa Share Code   |
      | Change Address link for Dependant 1         | Enter Dependant Address screen   | Same Address as Applicant screen | Dep With Same Address |

  Scenario Outline: Validate the back link functionality on Check Your Answers page by changing the answers
    And My details are captured until Check your answers screen with Dependant
    When I select <changeLink> on Check Your Answers page
    Then I will see the <screen>
    And My claimant's <answer> is pre-populated
    When I change my <answer> and do not continue
    And I select the Back link
    Then I will see the <previous screen>
    And I continue without changing my answer on <previous screen>
    Then I will see the Check Your Answers screen
    And My answer remains same as on <screen>

    Examples:
      | changeLink                                  | screen                           | previous screen                  | answer                |
      | Change Date of birth link                   | Date of birth screen             | Name screen                      | Date of birth         |
      | Change IHS number link                      | IHS Number screen                | Date of birth screen             | IHS Number            |
      | Change Visa Share Code link                 | Visa Share Code screen           | IHS Number screen                | Visa Share Code       |
      | Change Address link                         | Enter Address screen             | Visa Share Code screen           | Address               |
      | Change Email address link                   | Email Address screen             | Enter Address screen             | Email                 |
      | Change Phone number link                    | Phone number screen              | Email Address screen             | Phone Number          |
      | Change Name link for Dependant 1            | Dependant Name screen            | Dependant Living in UK screen    | Dep Name              |
      | Change Date of birth link for Dependant 1   | Dependant Date of birth screen   | Dependant Name screen            | Dep Date of Birth     |
      | Change IHS number link for Dependant 1      | Dependant IHS Number screen      | Dependant Date of birth screen   | Dep IHS Number        |
      | Change Visa Share Code link for Dependant 1 | Dependant Visa Share Code screen | Dependant IHS Number screen      | Dep Visa Share Code   |
      | Change Address link for Dependant 1         | Enter Dependant Address screen   | Same Address as Applicant screen | Dep With Same Address |

  Scenario Outline: Validate the change links on Check Your Answers page without Dependant - change the answer and verify answers are updated
    And My applicant details are captured until Check your answers screen
    When I select <changeLink> on Check Your Answers page
    Then I will see the <screen>
    When I change my <answer> and continue
    Then I will see the Check Your Answers screen
    And I see the updated <answer> on Check Your Answers screen

    Examples:
      | changeLink                  | screen                 | answer          |
      | Change Name link            | Name screen            | Name            |
      | Change Date of birth link   | Date of birth screen   | Date of birth   |
      | Change IHS number link      | IHS Number screen      | IHS Number      |
      | Change Visa Share Code link | Visa Share Code screen | Visa Share Code |
      | Change Address link         | Enter Address screen   | Address         |
      | Change Email address link   | Email Address screen   | Email           |
      | Change Phone number link    | Phone number screen    | Phone Number    |

  Scenario Outline: Validate the change links on Check Your Answers page with Dependant - change the answer and verify answers are updated
    And My details are captured until Check your answers screen with different address for Dependant
    When I select <changeLink> on Check Your Answers page
    Then I will see the <screen>
    When I change my <answer> and continue
    Then I will see the Check Your Answers screen
    And I see the updated <answer> on Check Your Answers screen

    Examples:
      | changeLink                                  | screen                           | answer                     |
      | Change Name link                            | Name screen                      | Name                       |
      | Change Date of birth link                   | Date of birth screen             | Date of birth              |
      | Change IHS number link                      | IHS Number screen                | IHS Number                 |
      | Change Visa Share Code link                 | Visa Share Code screen           | Visa Share Code            |
      | Change Address link                         | Enter Address screen             | Address                    |
      | Change Email address link                   | Email Address screen             | Email                      |
      | Change Phone number link                    | Phone number screen              | Phone Number               |
      | Change Name link for Dependant 1            | Dependant Name screen            | Dep Name                   |
      | Change Date of birth link for Dependant 1   | Dependant Date of birth screen   | Dep Date of Birth          |
      | Change IHS number link for Dependant 1      | Dependant IHS Number screen      | Dep IHS Number             |
      | Change Visa Share Code link for Dependant 1 | Dependant Visa Share Code screen | Dep Visa Share Code        |
      | Change Address link for Dependant 1         | Enter Dependant Address screen   | Dep With Different Address |

  Scenario Outline: Validate the Dependant Address change link on Check Your Answers page - change the address and verify address is updated
    And My details are captured until Check your answers screen with Dependant
    When I select <changeLink> on Check Your Answers page
    Then I will see the <screen>
    And My claimant's <address> is pre-populated
    When I change my <answer> and continue
    Then I will see the Check Your Answers screen
    And I see the updated <answer> on Check Your Answers screen

    Examples:
      | changeLink                          | screen                         | answer                     | address               |
      | Change Address link for Dependant 1 | Enter Dependant Address screen | Dep With Different Address | Dep With Same Address |

  Scenario Outline: Validate the Dependant Address change link on Check Your Answers page - change answer on Same Address screen and verify address remains same
    And My details are captured until Check your answers screen with Dependant
    When I select <changeLink> on Check Your Answers page
    Then I will see the <screen>
    And My claimant's <answer> is pre-populated
    When I select the Back link
    Then I will see the Same Address as Applicant screen
    When I change my <answer> and continue
    Then I will see the Check Your Answers screen
    And My answer remains same as on <screen>

    Examples:
      | changeLink                          | answer                | screen                          |
      | Change Address link for Dependant 1 | Dep With Same Address | Enter Dependant Address screen  |