@ViewEvidence @IHSRI-1096 @Regression

Feature: Validation of the Uploaded evidence page on the IHS claim app to enable student applicants to view their uploaded EHIC and CAS to prove their eligibility.

  Background:
    Given I launch the IHS Student claim application
    And My details are captured until Upload CAS screen

  @Smoke
  Scenario Outline: Validate the user is able to view the uploaded CAS file
    When I upload the CAS <fileName> of format <fileFormat>
    And I view the uploaded CAS <fileName>
    Then I will see the <output>
    Examples:
      | fileName             | fileFormat | output               |
      | Sample_4             | .bmp       | Add Dependant screen |
      | sample_3             | .pdf       | Add Dependant screen |
      | Payslip-June         | .png       | Add Dependant screen |
#      | Large_size_jpeg_file | .jpeg      | Add Dependant screen |
#      | Large_size_jpg_file  | .jpg       | Add Dependant screen |
#      | Large_size_png_file  | .png       | Add Dependant screen |

  Scenario Outline: Validate the hyperlinks on View CAS page
    When I <uploadFile> upload the CAS file
    And I select the <hyperlink>
    Then I will see the <output>
    Examples:
      | uploadFile | hyperlink         | output        |
      | Do         | Service Name link | Start screen  |
      | Do         | GOV.UK link       | GOV UK screen |



