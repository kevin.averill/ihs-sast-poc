@EnterAddress @IHSRI-1088 @Regression

Feature: Validation of the Enter Address page on IHS claim app to enable student applicants to enter their address for the claim application

  Background:
    Given I launch the IHS Student claim application
    And My details are captured until Enter Address screen

  Scenario Outline: Validate the enter address functionality for positive and negative scenarios
    When My address is <Building> <Street> <Town> <County> and <Postcode>
    Then I will see the <output>
    Examples:
      | Building         | Street        | Town        | County           | Postcode  | output                       |
      #positive tests
      | 123              | Test Street   | London      | Wiltshire        | EC1A 1BB  | Email Address screen         |
      | 123 Test Street  |               | London      |                  | W1A 0AX   | Email Address screen         |
      | 123, Test Street |               | London      | Wiltshire        | M1 1AE    | Email Address screen         |
      | 123              | Test's Street | London      |                  | B33 8TH   | Email Address screen         |
      | 123              | Test's Street | London-town | Wiltshire-county | CR2 6XH   | Email Address screen         |
      | 123              | Test's Street | London-town | Wiltshire-county | DN551PT   | Email Address screen         |
      #negative tests
      |                  |               | Swindon     | Wiltshire        | DN551PT   | Blank building error         |
      | 123              | Long Street   |             | Wiltshire        | DN551PT   | Blank town error             |
      | 123              | Long Street   | London      | Wiltshire        |           | Blank post code error        |
      | 123              | Long Street   | London      | Wiltshire        | SW1A2 1AA | Wrong post code error        |
      | 123              | Long Street   | London      | Wiltshire        | SW1A1AA12 | Wrong post code error        |
      | 123              | Long Street   | London      | Wiltshire        | TESTPOST  | Wrong post code error        |
      | b                |               | London      | Wiltshire        | W1A 0AX   | Invalid range building error |
      | 123              | s             | London      | Wiltshire        | B33 8TH   | Invalid range street error   |
      | 123              | Long Street   | t           | Wiltshire        | B33 8TH   | Invalid range town error     |
      | 123              | Long Street   | London      | c                | B33 8TH   | Invalid range county error   |
      | ***              | Test Street   | London      | Wiltshire        | W1A 0AX   | Invalid building error       |
      | 123              | "Test Street" | London      | Wiltshire        | W1A 0AX   | Invalid street error         |
      | 123              | Test Street   | London@123  | Wiltshire        | W1A 0AX   | Invalid town error           |
      | 123              | Test Street   | London      | (Wiltshire)      | W1A 0AX   | Invalid county error         |

  Scenario Outline: Validate the hyperlinks on Enter Address page
    When I select the <hyperlink>
    Then I will see the <output>
    Examples:
      | hyperlink         | output                 |
      | Back link         | Visa Share Code screen |
      | Service Name link | Start screen           |
      | GOV.UK link       | GOV UK screen          |