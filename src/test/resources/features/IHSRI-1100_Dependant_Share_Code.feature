@DependantVisaShareCode @IHSRI-1100 @Regression

Feature: Validation of Dependant Visa Share Code Page on the IHS claim app to enable student applicants to enter their dependant's visa share code for the claim application

  Background:
    Given I launch the IHS Student claim application
    And My details are captured until Dependant Visa Share Code screen

  Scenario Outline: Validate the enter visa share code functionality for positive and negative scenarios
    When My dependant's visa share code is <VisaCode>
    Then I will see the <output>
    Examples:
      | VisaCode      | output                             |
      #positive tests
      | A12 345 67Z   | Same Address as Applicant screen     |
      | a12 a45 a7z   | Same Address as Applicant screen     |
      | A00 A00 A00   | Same Address as Applicant screen     |
      | A1234567Z     | Same Address as Applicant screen     |
      | A12A45A7Z     | Same Address as Applicant screen     |
      | 123456789     | Same Address as Applicant screen     |
      | ABCDEFGHA     | Same Address as Applicant screen     |
      | asdfghjkl     | Same Address as Applicant screen     |
      | Aaa aaa aaZ   | Same Address as Applicant screen	 |
      | 012 345 678   | Same Address as Applicant screen	 |
      | A12 345 678   | Same Address as Applicant screen 	 |
      | 112 345 67Z   | Same Address as Applicant screen 	 |
      | a66 B37 b34   | Same Address as Applicant screen	 |
      | aaa bbb ccc   | Same Address as Applicant screen	 |
      | AAA 111 CCC   | Same Address as Applicant screen	 |
      | A1A 1A1 C1C   | Same Address as Applicant screen	 |

      #negative tests
      |               | Blank dependant share code error   |
      | A12-345-67Z   | Invalid dependant share code error |
      | A12:345:67Z   | Invalid dependant share code error |
      | '12 345 67'   | Invalid dependant share code error |
      | A12 345 678Z  | Invalid dependant share code error |
      | A12 34 67Z    | Invalid dependant share code error |
      | A1134#67Z     | Invalid dependant share code error |
      | A1@34@67Z     | Invalid dependant share code error |
      | ?12 345 67?   | Invalid dependant share code error |
      | A12 345 67&   | Invalid dependant share code error |
      | A12 34$ 67Z   | Invalid dependant share code error |

  Scenario Outline: Validate the hyperlinks on Dependant Visa Share Code page
    When I select the <hyperlink>
    Then I will see the <output>
    Examples:
      | hyperlink              | output                       |
      | Back link              | Dependant IHS Number screen  |
      | Service Name link      | Start screen                 |
      | GOV.UK link            | GOV UK screen                |
      | Share code GOV.UK link | GOV UK View and Prove screen |
