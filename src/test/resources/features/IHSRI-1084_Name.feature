@Name @IHSRI-1084 @Regression

Feature: Validation of the Name Page on IHS claim app to enable student applicants to enter their given name and family name for the claim application

  Background:
    Given I launch the IHS Student claim application

  @Retest-1289 @Retest-1291
  Scenario Outline: Validate the enter name functionality for positive and negative scenarios
    When My firstname is <GivenName> and surname is <FamilyName>
    Then I will see the <output>
    Examples:
      | GivenName            | FamilyName            | output                          |
      #positive tests
      | firstName            | lastName              | Date of birth screen            |
      | firstName            | O'hara                | Date of birth screen            |
      | firstName-middleName | lastName              | Date of birth screen            |
      | FirstName middleName | LastName              | Date of birth screen            |
      | firstName            | middleName lastName   | Date of birth screen            |
      | fn                   | ln                    | Date of birth screen            |
      #negative tests
      |                      | lastName              | Blank given name error          |
      | firstName            |                       | Blank family name error         |
      | (first name)         | last name             | Invalid given name error        |
      | first name 1         | last name             | Invalid given name error        |
      | first name           | "last name"           | Invalid family name error       |
      | first name           | last name 123         | Invalid family name error       |
      | A                    | last name             | Invalid range given name error  |
      | first name           | a                     | Invalid range family name error |

  Scenario Outline: Validate user is not allowed to enter name more than 50 characters
    When I attempt to enter <count> characters name
    Then I am restricted to enter <maximum> characters name
    And I will see the <output>
    Examples:
      | count | maximum | output               |
      | 51    | 50      | Date of birth screen |

  Scenario Outline: Validate the hyperlinks on Name page
    When I select the <hyperlink>
    Then I will see the <output>
    Examples:
      | hyperlink         | output          |
      | Service Name link | Start screen    |
      | GOV.UK link       | GOV UK screen   |