@VisaShareCode @IHSRI-1087 @Regression

Feature: Validation of Visa Share Code Page on the IHS claim app to enable student applicants to enter their visa share code for the claim application

  Background:
    Given I launch the IHS Student claim application
    And My details are captured until Visa Share Code screen

  @Retest-1296 @Retest-1297 @Retest-1449
  Scenario Outline: Validate the enter visa share code functionality for positive and negative scenarios
    When My visa share code is <VisaCode>
    Then I will see the <output>
    Examples:
      | VisaCode      | output                   |
      #positive tests
      | A12 345 67Z   | Enter Address screen     |
      | a12 a45 a7z   | Enter Address screen     |
      | A00 A00 A00   | Enter Address screen     |
      | A1234567Z     | Enter Address screen     |
      | A12A45A7Z     | Enter Address screen     |
      | 123456789     | Enter Address screen     |
      | ABCDEFGHA     | Enter Address screen     |
      | asdfghjkl     | Enter Address screen     |
      | Aaa aaa aaZ   | Enter Address screen 	 |
      | 012 345 678   | Enter Address screen	 |
      | A12 345 678   | Enter Address screen 	 |
      | 112 345 67Z   | Enter Address screen	 |
      | a66 B37 b34   | Enter Address screen	 |
      | aaa bbb ccc   | Enter Address screen	 |
      | AAA 111 CCC   | Enter Address screen	 |
      | A1A 1A1 C1C   | Enter Address screen	 |
      #negative tests
      |               | Blank share code error   |
      | A12-345-67Z   | Invalid share code error |
      | A12:345:67Z   | Invalid share code error |
      | '12 345 67'   | Invalid share code error |
      | A12 345 678Z  | Invalid share code error |
      | A12 34 67Z    | Invalid share code error |
      | A1134#67Z     | Invalid share code error |
      | A1@34@67Z     | Invalid share code error |
      | ?12 345 67?   | Invalid share code error |
      | A12 345 67&   | Invalid share code error |
      | A12 34$ 67Z   | Invalid share code error |

  Scenario Outline: Validate the hyperlinks on Visa Share Code page
    When I select the <hyperlink>
    Then I will see the <output>
    Examples:
      | hyperlink              | output                       |
      | Back link              | IHS Number screen            |
      | Service Name link      | Start screen                 |
      | GOV.UK link            | GOV UK screen                |
      | Share code GOV.UK link | GOV UK View and Prove screen |