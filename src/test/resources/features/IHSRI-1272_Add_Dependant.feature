@IHSRI-1272 @AddDependant @Regression

Feature: Validation of the Add Dependant Page on the IHS claim app to enable student applicants to select if they have dependants to add

  Background:
    Given I launch the IHS Student claim application
    And My details are captured until Add Dependant screen

  Scenario Outline: Validate the user is able to add dependants to the claim application
    When I <dependantOption> have dependant to add
    Then I will see the <output>
    Examples:
      | dependantOption | output                     |
      | do              | Dependant Paid Work screen |
      | do not          | Check Your Answers screen  |
      |                 | Dependant question error   |

  Scenario Outline: Validate the hyperlinks on Add Dependant page
    When I select the <hyperlink>
    Then I will see the <output>
    Examples:
      | hyperlink         | output                        |
      | Back link         | View Uploaded Evidence screen |
      | Service Name link | Start screen                  |
      | GOV.UK link       | GOV UK screen                 |