@DependantAddress @IHSRI-1102 @Regression

Feature: Validation of the Enter Dependant Address page on IHS claim app to enable student applicants to enter their dependant's address for the claim application

  Background:
    Given I launch the IHS Student claim application
    And My details are captured until Dependant Address screen

  Scenario Outline: Validate the enter address functionality for positive and negative scenarios
    When My dependant's address is <Building> <Street> <Town> <County> and <Postcode>
    Then I will see the <output>
    Examples:
      | Building         | Street        | Town        | County           | Postcode  | output                                 |
      #positive tests
      | 123              | Test Street   | London      | Wiltshire        | EC1A 1BB  | Upload Dependant EHIC Card screen      |
      | 123 Test Street  |               | London      |                  | W1A 0AX   | Upload Dependant EHIC Card screen      |
      | 123, Test Street |               | London      | Wiltshire        | M1 1AE    | Upload Dependant EHIC Card screen      |
      | 123              | Test's Street | London      |                  | B33 8TH   | Upload Dependant EHIC Card screen      |
      | 123              | Test's Street | London-town | Wiltshire-county | CR2 6XH   | Upload Dependant EHIC Card screen      |
      | 123              | Test's Street | London-town | Wiltshire-county | DN551PT   | Upload Dependant EHIC Card screen      |
      #negative tests
      |                  |               | Swindon     | Wiltshire        | DN551PT   | Blank dependant building error         |
      | 123              | Long Street   |             | Wiltshire        | DN551PT   | Blank dependant town error             |
      | 123              | Long Street   | London      | Wiltshire        |           | Blank dependant post code error        |
      | 123              | Long Street   | London      | Wiltshire        | SW1A2 1AA | Wrong dependant post code error        |
      | 123              | Long Street   | London      | Wiltshire        | SW1A1AA12 | Wrong dependant post code error        |
      | 123              | Long Street   | London      | Wiltshire        | TESTPOST  | Wrong dependant post code error        |
      | b                |               | London      | Wiltshire        | W1A 0AX   | Invalid range dependant building error |
      | 123              | s             | London      | Wiltshire        | B33 8TH   | Invalid range dependant street error   |
      | 123              | Long Street   | t           | Wiltshire        | B33 8TH   | Invalid range dependant town error     |
      | 123              | Long Street   | London      | c                | B33 8TH   | Invalid range dependant county error   |
      | ***              | Test Street   | London      | Wiltshire        | W1A 0AX   | Invalid dependant building error       |
      | 123              | "Test Street" | London      | Wiltshire        | W1A 0AX   | Invalid dependant street error         |
      | 123              | Test Street   | London@123  | Wiltshire        | W1A 0AX   | Invalid dependant town error           |
      | 123              | Test Street   | London      | (Wiltshire)      | W1A 0AX   | Invalid dependant county error         |

  Scenario Outline: Validate the hyperlinks on Enter Dependant Address page
    When I select the <hyperlink>
    Then I will see the <output>
    Examples:
      | hyperlink         | output                           |
      | Back link         | Same Address as Applicant screen |
      | Service Name link | Start screen                     |
      | GOV.UK link       | GOV UK screen                    |